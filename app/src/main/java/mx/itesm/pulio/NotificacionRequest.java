package mx.itesm.pulio;

public class NotificacionRequest extends Notificacion {

    //Numero de parada en la que se el usuario solicitó inscribirse
    private final int numParada;
    private final Ruta ruta;

    public NotificacionRequest(String idRemitente, String idDestinatario, Ruta ruta, int numParada) {
        super(idRemitente, idDestinatario, true);
        this.numParada = numParada;
        this.ruta = ruta;
    }

    public int getNumParada() {
        return numParada;
    }

    public Ruta getRuta() {
        return ruta;
    }

    @Override
    public String toString() {
        return "NotificacionRequest{" + "\n" +
                "numParada=" + numParada + "\n" +
                ", ruta=" + ruta + "\n" +
                '}';
    }

}
