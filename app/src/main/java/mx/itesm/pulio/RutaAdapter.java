package mx.itesm.pulio;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.chad.pulioapp.R;

import java.util.ArrayList;
import java.util.List;

public class RutaAdapter extends RecyclerView.Adapter<RutaAdapter.CardViewHolder> {

    private Context mCtx;
    private List<Ruta> rutaList;
    private List<Ruta> rutaSeleccionadaList;
    private OnItemClickListener mListener;

    public RutaAdapter(Context mCtx, List<Ruta> rutaList, List<Ruta> rutaSeleccionadaList) {
        this.mCtx = mCtx;
        this.rutaList = rutaList;
        this.rutaSeleccionadaList = rutaSeleccionadaList;

    }


    public interface OnItemClickListener{
        void onItemClick(int position);
        void onSelectClick(int position);
    }
    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;

    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =  inflater.inflate(R.layout.ruta_elegir_layout,null);
        return new CardViewHolder(view,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, final int position) {

        Ruta ruta = rutaList.get(position);
        llenarInfoRuta(holder,ruta,position);

    }

    @Override
    public int getItemCount() {
        return rutaList.size();
    }

    class CardViewHolder extends RecyclerView.ViewHolder{


        CardView cvRuta;
        TextView tvDiaRutaElegir, tvNombreChofer, tvHoraRuta2,
                tvDirDestino, tvDispRuta, tvDistancia;

        LinearLayout linlayHeader;

        Switch swRuta;

        public CardViewHolder(View itemView, OnItemClickListener listener) {

            super(itemView);

            cvRuta = itemView.findViewById(R.id.cvRuta);
            tvDiaRutaElegir = itemView.findViewById(R.id.tvDiaRutaElegir);
            linlayHeader = itemView.findViewById(R.id.linlayHeader);
            swRuta = itemView.findViewById(R.id.swRuta);
            tvNombreChofer = itemView.findViewById(R.id.tvNombreChofer);
            tvHoraRuta2 = itemView.findViewById(R.id.tvHoraRuta);
            tvDispRuta = itemView.findViewById(R.id.tvDispRuta);

            tvDirDestino = itemView.findViewById(R.id.tvDirDestino);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener !=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }

                }
            });
            swRuta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (listener !=null){
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION){
                            listener.onSelectClick(position);
                        }
                    }

                }
            });

        }
    }
    public void filterList(ArrayList<Ruta> filteredList) {
        rutaList = filteredList;
        notifyDataSetChanged();

    }
    private void llenarInfoRuta(CardViewHolder holder,Ruta ruta, int position){

        //Dia
        holder.tvDiaRutaElegir.setText(String.valueOf(rutaList.get(position).getDia()));

        //Chofer
        holder.tvNombreChofer.setText(rutaList.get(position).getChofer().getNombre());

        //Hora
        String hora = String.format("%02d:%02d",rutaList.get(position).getParadas().get(0).getHora(),rutaList.get(position).getParadas().get(0).getMinuto());
        holder.tvHoraRuta2.setText(hora);

        //Lugar/Direccion
        holder.tvDirDestino.setText(rutaList.get(position).getParadas().get(rutaList.get(position).getParadas().size()-1).getLugar().getDireccion());

        //Disponibilidad
        String lugares = String.valueOf(rutaList.get(position).getChofer().getCapacidadAuto());
        String inscritos = String.valueOf(rutaList.get(position).getInscritos());
        inscritos = inscritos.concat("/").concat(lugares);
        holder.tvDispRuta.setText(inscritos);

        //Cambiar de Color dependiendo de ida o vuelta
        if (rutaList.get(position).getIsLlegada()) {
            holder.linlayHeader.setBackgroundColor(ContextCompat.getColor(mCtx,R.color.colorAccent));
        }else {
            holder.linlayHeader.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.colorOrange));
        }

        //Click listener
        holder.swRuta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rutaList.get(position).getId();
            }
        });
        //Ruta Seleccionada o No
        holder.swRuta.setChecked(rutaSeleccionadaList.contains(rutaList.get(position)));

    }




    }
