package mx.itesm.pulio;

public abstract class Notificacion {

    private final String idRemitente;
    private final String idDestinatario;
    //Hay dos tipos de notificaciones request o acknowledege. Si la variable isRequest es
    //true es de tipo request, si es false entonces es de tipo acknowledge
    private final boolean isRequest;

    protected Notificacion(String idRemitente, String idDestinatario, boolean isRequest) {
        this.idRemitente = idRemitente;
        this.idDestinatario = idDestinatario;
        this.isRequest = isRequest;
    }

    public String getIdRemitente() {
        return idRemitente;
    }

    public String getIdDestinatario() {
        return idDestinatario;
    }

    public boolean isRequest() {
        return isRequest;
    }
}
