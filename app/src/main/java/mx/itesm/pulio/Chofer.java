package mx.itesm.pulio;

import java.util.HashMap;
import java.util.Map;

public class Chofer {

    private final String id;
    private final String nombre;
    private final long telefono;
    private final int capacidadAuto;

    public Chofer(String id, String nombre, long telefono, int capacidadAuto) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.capacidadAuto = capacidadAuto;
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public long getTelefono() {
        return telefono;
    }

    public int getCapacidadAuto() {
        return capacidadAuto;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("choferId", this.getId());
        map.put("choferNombre", this.getNombre());
        map.put("choferTelefono", this.getTelefono());
        map.put("choferCapacidadAuto", this.getCapacidadAuto());
        return map;
    }

    @Override
    public String toString() {
        return "Chofer{" + "\n" +
                "id=" + id + "\n" +
                ", nombre=" + nombre + "\n" +
                ", telefono=" + telefono + "\n" +
                ", capacidadAuto=" + capacidadAuto + "\n" +
                '}';
    }
}
