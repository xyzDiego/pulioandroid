package mx.itesm.pulio;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

public abstract class TiempoQuery extends Thread {

    private static final int DAYS_OF_WEEK = 7;
    private static final int CONVERSION_FACTOR = 5;
    private static final long SECONDS_PER_DAY = 60 * 60 * 24;

    private final double origenLatitud;
    private final double origenLongitud;
    private final double destinoLatitud;
    private final double destinoLongitud;
    private final DiaDeLaSemana diaDeLaSemana;

    private double distanciaKm;
    private StatusRequest status;

    protected TiempoQuery(double origenLatitud, double origenLongitud, double destinoLatitud,
                          double destinoLongitud, DiaDeLaSemana diaDeLaSemana) {
        this.origenLatitud = origenLatitud;
        this.origenLongitud = origenLongitud;
        this.destinoLatitud = destinoLatitud;
        this.destinoLongitud = destinoLongitud;
        this.diaDeLaSemana = diaDeLaSemana;
    }

    @Override
    public void run() {
        try {
            Calendar calendar = Calendar.getInstance();

            int diaActual = (calendar.get(Calendar.DAY_OF_WEEK) + CONVERSION_FACTOR) % DAYS_OF_WEEK;
            int diaDeRuta = this.diaDeLaSemana.ordinal();

            int diasQueFaltanParaLaRuta = diaActual < diaDeRuta ? diaDeRuta - diaActual : diaDeRuta + DAYS_OF_WEEK - diaActual;
            long segundos = (diasQueFaltanParaLaRuta * SECONDS_PER_DAY) + (calendar.getTimeInMillis() / 1000);

            calendar.setTimeInMillis(segundos * 1000);

            String targetURL = "https://maps.googleapis.com/maps/api/directions/json?" +
                    "origin=" + this.origenLatitud + "," + this.origenLongitud +
                    "&destination=" + this.destinoLatitud + "," + this.destinoLongitud +
                    "&departure_time=" + segundos +
                    "&key=AIzaSyBl0ZhkjDBRYeNRwVOfDmrc-VcPhDA-FNg";

            URL url = new URL(targetURL);
            URLConnection conn = url.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder response = new StringBuilder();

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json = new JSONObject(response.toString());

            String statusRequest = json.getString("status");
            obtenerInformationStatus(statusRequest);

            if (this.status == StatusRequest.OK) {
                JSONObject directionsInformation = json.getJSONArray("routes").getJSONObject(0).
                        getJSONArray("legs").getJSONObject(0);
                String strDistance = directionsInformation.getJSONObject("distance").getString("text");
                String strDuration = directionsInformation.getJSONObject("duration_in_traffic").getString("text");

                obtenerInformacionDistancia(strDistance);
                obtenerInformacionDuracion(strDuration);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            this.status = StatusRequest.MALFORMED_URL;
        } catch (IOException e) {
            e.printStackTrace();
            this.status = StatusRequest.IO_EXCEPTION;
        } catch (Exception e) {
            e.printStackTrace();
            this.status = StatusRequest.UNKNOWN_EXCEPTION;
        }
    }

    public void obtenerInformacionDistancia(String distance) {
        String measure = distance.split(" ")[1];

        if (measure.equals("km")) {
            this.distanciaKm = Double.parseDouble(distance.split(" ")[0]);
        } else {
            double distanciaM = Double.parseDouble(distance.split(" ")[0]);
            this.distanciaKm = distanciaM / 1000;
        }
    }

    public void obtenerInformacionDuracion(String strDuration) {}

    public void obtenerInformationStatus(String statusRequest) {
        switch (statusRequest) {
            case "OK":
                this.status = StatusRequest.OK;
                break;
            case "ZERO_RESULTS":
                this.status = StatusRequest.ZERO_RESULTS;
                break;
            case "OVER_QUERY_LIMIT":
                this.status = StatusRequest.OVER_QUERY_LIMIT;
                break;
            case "REQUEST_DENIED":
                this.status = StatusRequest.REQUEST_DENIED;
                break;
            case "INVALID_REQUEST":
                this.status = StatusRequest.INVALID_REQUEST;
                break;
            default:
                this.status = StatusRequest.UNKNOWN_ERROR;
                break;
        }
    }

    public double getDistanciaKm() {
        return distanciaKm;
    }

    public StatusRequest getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "TiempoQuery{" +
                "origenLatitud=" + origenLatitud +
                ", origenLongitud=" + origenLongitud +
                ", destinoLatitud=" + destinoLatitud +
                ", destinoLongitud=" + destinoLongitud +
                ", diaDeLaSemana=" + diaDeLaSemana +
                ", distanciaKm=" + distanciaKm +
                ", status=" + status.name() +
                '}';
    }
}
