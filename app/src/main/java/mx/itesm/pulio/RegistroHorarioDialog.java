package mx.itesm.pulio;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.chad.pulioapp.R;

import java.util.ArrayList;
import java.util.Arrays;

public class RegistroHorarioDialog extends AppCompatDialogFragment {

    private final ToggleButton[] toggleButtons = new ToggleButton[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
    private final Spinner[] spinnersLugarLlegada = new Spinner[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
    private final Spinner[] spinnersLugarSalida = new Spinner[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
    private final Spinner[] spinnersHoraLlegada = new Spinner[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
    private final Spinner[] spinnersHoraSalida = new Spinner[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
    private final ImageView[] imageViewsCarros = new ImageView[DiaDeLaSemana.TOTAL_DIAS.ordinal()];

    private final Usuario usuario = DataHolder.getInstance().getUsuario();
    private final Automovil automovil = usuario.getAutomovil();
    private final ArrayList<Lugar> lugares = usuario.getLugares();
    private final Horario[] horariosLlegada = usuario.getHorariosLlegada();
    private final Horario[] horariosSalida = usuario.getHorariosSalida();

    private final String[] HORARIOS = {
            "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15",
            "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45",
            "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15",
            "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45",
            "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15",
            "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45",
            "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15",
            "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"};
    private final ArrayList<String> nombresLugares = new ArrayList<>();

    private RegistroHorarioListener rhListener;

    @SuppressLint("NewApi")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_registro_horario, null);

        //1. Obtener referencias de la vista e inicializar arreglos
        toggleButtons[DiaDeLaSemana.LUNES.ordinal()] = dialogView.findViewById(R.id.togTrabajoLunes);
        toggleButtons[DiaDeLaSemana.MARTES.ordinal()] = dialogView.findViewById(R.id.togTrabajoMartes);
        toggleButtons[DiaDeLaSemana.MIERCOLES.ordinal()] = dialogView.findViewById(R.id.togTrabajoMiercoles);
        toggleButtons[DiaDeLaSemana.JUEVES.ordinal()] = dialogView.findViewById(R.id.togTrabajoJueves);
        toggleButtons[DiaDeLaSemana.VIERNES.ordinal()] = dialogView.findViewById(R.id.togTrabajoViernes);
        toggleButtons[DiaDeLaSemana.SABADO.ordinal()] = dialogView.findViewById(R.id.togTrabajoSabado);

        spinnersLugarLlegada[DiaDeLaSemana.LUNES.ordinal()] = dialogView.findViewById(R.id.spnLunesLugarLlegada);
        spinnersLugarLlegada[DiaDeLaSemana.MARTES.ordinal()] = dialogView.findViewById(R.id.spnMartesLugarLlegada);
        spinnersLugarLlegada[DiaDeLaSemana.MIERCOLES.ordinal()] = dialogView.findViewById(R.id.spnMiercolesLugarLlegada);
        spinnersLugarLlegada[DiaDeLaSemana.JUEVES.ordinal()] = dialogView.findViewById(R.id.spnJuevesLugarLlegada);
        spinnersLugarLlegada[DiaDeLaSemana.VIERNES.ordinal()] = dialogView.findViewById(R.id.spnViernesLugarLlegada);
        spinnersLugarLlegada[DiaDeLaSemana.SABADO.ordinal()] = dialogView.findViewById(R.id.spnSabadoLugarLlegada);

        spinnersLugarSalida[DiaDeLaSemana.LUNES.ordinal()] = dialogView.findViewById(R.id.spnLunesLugarSalida);
        spinnersLugarSalida[DiaDeLaSemana.MARTES.ordinal()] = dialogView.findViewById(R.id.spnMartesLugarSalida);
        spinnersLugarSalida[DiaDeLaSemana.MIERCOLES.ordinal()] = dialogView.findViewById(R.id.spnMiercolesLugarSalida);
        spinnersLugarSalida[DiaDeLaSemana.JUEVES.ordinal()] = dialogView.findViewById(R.id.spnJuevesLugarSalida);
        spinnersLugarSalida[DiaDeLaSemana.VIERNES.ordinal()] = dialogView.findViewById(R.id.spnViernesLugarSalida);
        spinnersLugarSalida[DiaDeLaSemana.SABADO.ordinal()] = dialogView.findViewById(R.id.spnSabadoLugarSalida);

        spinnersHoraLlegada[DiaDeLaSemana.LUNES.ordinal()] = dialogView.findViewById(R.id.spnLunesHoraLlegada);
        spinnersHoraLlegada[DiaDeLaSemana.MARTES.ordinal()] = dialogView.findViewById(R.id.spnMartesHoraLlegada);
        spinnersHoraLlegada[DiaDeLaSemana.MIERCOLES.ordinal()] = dialogView.findViewById(R.id.spnMiercolesHoraLlegada);
        spinnersHoraLlegada[DiaDeLaSemana.JUEVES.ordinal()] = dialogView.findViewById(R.id.spnJuevesHoraLlegada);
        spinnersHoraLlegada[DiaDeLaSemana.VIERNES.ordinal()] = dialogView.findViewById(R.id.spnViernesHoraLlegada);
        spinnersHoraLlegada[DiaDeLaSemana.SABADO.ordinal()] = dialogView.findViewById(R.id.spnSabadoHoraLlegada);

        spinnersHoraSalida[DiaDeLaSemana.LUNES.ordinal()] = dialogView.findViewById(R.id.spnLunesHoraSalida);
        spinnersHoraSalida[DiaDeLaSemana.MARTES.ordinal()] = dialogView.findViewById(R.id.spnMartesHoraSalida);
        spinnersHoraSalida[DiaDeLaSemana.MIERCOLES.ordinal()] = dialogView.findViewById(R.id.spnMiercolesHoraSalida);
        spinnersHoraSalida[DiaDeLaSemana.JUEVES.ordinal()] = dialogView.findViewById(R.id.spnJuevesHoraSalida);
        spinnersHoraSalida[DiaDeLaSemana.VIERNES.ordinal()] = dialogView.findViewById(R.id.spnViernesHoraSalida);
        spinnersHoraSalida[DiaDeLaSemana.SABADO.ordinal()] = dialogView.findViewById(R.id.spnSabadoHoraSalida);

        imageViewsCarros[DiaDeLaSemana.LUNES.ordinal()] = dialogView.findViewById(R.id.imgAutoLunes);
        imageViewsCarros[DiaDeLaSemana.MARTES.ordinal()] = dialogView.findViewById(R.id.imgAutoMartes);
        imageViewsCarros[DiaDeLaSemana.MIERCOLES.ordinal()] = dialogView.findViewById(R.id.imgAutoMiercoles);
        imageViewsCarros[DiaDeLaSemana.JUEVES.ordinal()] = dialogView.findViewById(R.id.imgAutoJueves);
        imageViewsCarros[DiaDeLaSemana.VIERNES.ordinal()] = dialogView.findViewById(R.id.imgAutoViernes);
        imageViewsCarros[DiaDeLaSemana.SABADO.ordinal()] = dialogView.findViewById(R.id.imgAutoSabado);

        //2. Poblar spinners
        ArrayAdapter<String> arrayAdapterHorarios = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, Arrays.asList(HORARIOS));
        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
            spinnersHoraLlegada[dia].setAdapter(arrayAdapterHorarios);
            spinnersHoraSalida[dia].setAdapter(arrayAdapterHorarios);
        }

        if (this.lugares.isEmpty()) {
            nombresLugares.add("Sin lugares registrados");
        } else {
            for (Lugar lugar: this.lugares)
                nombresLugares.add(lugar.getId());
        }
        ArrayAdapter<String> arrayAdapterLugares = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, nombresLugares);
        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
            spinnersLugarLlegada[dia].setAdapter(arrayAdapterLugares);
            spinnersLugarSalida[dia].setAdapter(arrayAdapterLugares);
        }

        //3. Poner los contenidos de la vista conforme al último estado almacenado
        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
            this.toggleButtons[dia].setChecked(this.automovil.getDisponibilidadPorDia()[dia] ||
                    (this.horariosSalida[dia] != null));

            if (this.toggleButtons[dia].isChecked()) {
                if (this.horariosSalida[dia] != null) {
                    this.spinnersHoraSalida[dia].setSelection(indexOfHorario(this.horariosSalida[dia]));
                    this.spinnersLugarSalida[dia].setSelection(indexOfLugar(this.horariosSalida[dia].getLugar()));
                }
                if (this.horariosLlegada[dia] != null) {
                    this.spinnersHoraLlegada[dia].setSelection(indexOfHorario(this.horariosLlegada[dia]));
                    this.spinnersLugarLlegada[dia].setSelection(indexOfLugar(this.horariosLlegada[dia].getLugar()));
                }
                if (!this.automovil.getDisponibilidadPorDia()[dia])
                    this.imageViewsCarros[dia].setVisibility(View.INVISIBLE);

            } else {
                this.spinnersLugarSalida[dia].setEnabled(false);
                this.spinnersLugarLlegada[dia].setEnabled(false);
                this.spinnersHoraSalida[dia].setEnabled(false);
                this.spinnersHoraLlegada[dia].setEnabled(false);
                this.imageViewsCarros[dia].setVisibility(View.INVISIBLE);
            }
        }

        //4. Set check listeners para los toogle buttons
        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
            toggleButtons[dia].setOnCheckedChangeListener(new ToogleCheckListener(dia));
        }

        //5. Set item select listeners para los spinners
        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
            spinnersHoraLlegada[dia].setOnItemSelectedListener(new SpinnerLlegadaItemSelectedListener(dia));
            spinnersHoraSalida[dia].setOnItemSelectedListener(new SpinnerSalidaItemSelectedListener(dia));
        }

        //6. Registrar horarios tras hacer click en botón de confirmar
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Registro de Horario")
                .setView(dialogView)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
                            setNuevoHorarioLlegada(dia);
                            setNuevoHorarioSalida(dia);
                        }

                        rhListener.checarDatosHorario();
                    }
                });

        return builder.create();
    }

    private void setNuevoHorarioLlegada(int diaDeLaSemana) {
        if (toggleButtons[diaDeLaSemana].isChecked()) {
                Lugar lugar = usuario.searchLugar(spinnersLugarLlegada[diaDeLaSemana].getSelectedItem().toString());
                String hora = spinnersHoraLlegada[diaDeLaSemana].getSelectedItem().toString();
                horariosLlegada[diaDeLaSemana] = new Horario(lugar, hora);
        } else {
            horariosLlegada[diaDeLaSemana] = null;
        }
    }

    private void setNuevoHorarioSalida(int diaDeLaSemana) {
        if (toggleButtons[diaDeLaSemana].isChecked()) {
            Lugar lugar = usuario.searchLugar(spinnersLugarSalida[diaDeLaSemana].getSelectedItem().toString());
            String hora = spinnersHoraSalida[diaDeLaSemana].getSelectedItem().toString();
            horariosSalida[diaDeLaSemana] = new Horario(lugar, hora);
        } else {
            horariosSalida[diaDeLaSemana] = null;
        }
    }

    private int indexOfHorario(Horario horario) {
        for (int i = 0; i < HORARIOS.length; i++) {
            if (HORARIOS[i].equals(horario.formatTime()))
                return i;
        }

        return -1;
    }

    private int indexOfLugar(Lugar lugar) {
        if (lugar == null)
            return 0;

        for (int i = 0; i < nombresLugares.size(); i++) {
            if (nombresLugares.get(i).equals(lugar.getId()))
                return i;
        }

        return -1;
    }

    private class ToogleCheckListener implements CompoundButton.OnCheckedChangeListener {

        private int dia;

        public ToogleCheckListener(int dia) {
            this.dia = dia;
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            if (automovil.getDisponibilidadPorDia()[dia] && !b) {
                Toast.makeText(getContext(), "No puedes ausentarte este día ya que has indicado que utilizas tu auto",
                        Toast.LENGTH_LONG).show();
                toggleButtons[dia].setChecked(true);
            } else {
                spinnersLugarSalida[dia].setEnabled(b);
                spinnersLugarLlegada[dia].setEnabled(b);
                spinnersHoraSalida[dia].setEnabled(b);
                spinnersHoraLlegada[dia].setEnabled(b);

                if (spinnersHoraSalida[dia].getSelectedItemId() == 0)
                    spinnersHoraSalida[dia].setSelection(1);
            }
        }
    }

    private class SpinnerLlegadaItemSelectedListener implements AdapterView.OnItemSelectedListener {

        private int dia;

        public SpinnerLlegadaItemSelectedListener(int dia) {
            this.dia = dia;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i >= spinnersHoraSalida[dia].getSelectedItemId() && toggleButtons[dia].isChecked()) {
                spinnersHoraSalida[dia].setSelection(i == HORARIOS.length - 1 ? i : i + 1);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) { }
    }

    private class SpinnerSalidaItemSelectedListener implements AdapterView.OnItemSelectedListener {

        private int dia;

        public SpinnerSalidaItemSelectedListener(int dia) {
            this.dia = dia;
        }

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            if (i <= spinnersHoraLlegada[dia].getSelectedItemId()) {
                spinnersHoraLlegada[dia].setSelection(i == 0 ? i : i - 1);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) { }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.rhListener = (RegistroHorarioListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +  " must implement RegistroHorarioListener");
        }
    }

    public interface RegistroHorarioListener {
        boolean checarDatosHorario();
    }

}
