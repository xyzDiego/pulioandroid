package mx.itesm.pulio;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.chad.pulioapp.R;

import java.util.Arrays;

public class RegistroAutoDialog extends AppCompatDialogFragment {

    private EditText etModeloAuto;
    private CheckBox cbAuto;
    private Spinner spnAnio;
    private Spinner spnCapacidad;
    private ToggleButton togLunes;
    private ToggleButton togMartes;
    private ToggleButton togMiercoles;
    private ToggleButton togJueves;
    private ToggleButton togViernes;
    private ToggleButton togSabado;

    private final String[] ANIOS = {"2019", "2018", "2017", "2016", "2015", "2014", "2013",
    "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002",
    "2001", "2000"};
    private final String[] CAPACIDADES = {"1", "2", "3", "4", "5", "6"};

    private RegistroAutoListener raListener;
    private Automovil automovil;

    @SuppressLint("NewApi")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_registro_auto, null);

        cbAuto = dialogView.findViewById(R.id.cbAuto);
        etModeloAuto = dialogView.findViewById(R.id.etModeloAuto);
        spnAnio = dialogView.findViewById(R.id.spnAnio);
        spnCapacidad = dialogView.findViewById(R.id.spnCapacidad);
        togLunes = dialogView.findViewById(R.id.togLunes);
        togMartes = dialogView.findViewById(R.id.togMartes);
        togMiercoles = dialogView.findViewById(R.id.togMiercoles);
        togJueves = dialogView.findViewById(R.id.togJueves);
        togViernes = dialogView.findViewById(R.id.togViernes);
        togSabado = dialogView.findViewById(R.id.togSabado);

        ArrayAdapter<String> arrayAdapterAnios = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, Arrays.asList(ANIOS));
        spnAnio.setAdapter(arrayAdapterAnios);

        ArrayAdapter<String> arrayAdapterCapacidades = new ArrayAdapter<>(getContext(),
                android.R.layout.simple_spinner_dropdown_item, Arrays.asList(CAPACIDADES));
        spnCapacidad.setAdapter(arrayAdapterCapacidades);

        this.automovil = DataHolder.getInstance().getUsuario().getAutomovil();

        // La manera más rápida de verificar si el usuario ya tiene automóvil es verificando
        // que alguna variable de instancia excepto el id (Ej. modelo) sea diferente de null.
        if (this.automovil.getModelo() != null) {
            // Si el usuario ya tiene auto, se restuaran todos los elementos del dialog conforme
            // a los datos guardados del objeto automóvil.
            cbAuto.setChecked(true);
            etModeloAuto.setText(this.automovil.getModelo());
            spnAnio.setSelection(indexOfAnio("" + this.automovil.getAnio()));
            spnCapacidad.setSelection(indexOfCapacidad("" + this.automovil.getCapacidad()));
            togLunes.setChecked(this.automovil.getDisponibilidadPorDia()[DiaDeLaSemana.LUNES.ordinal()]);
            togMartes.setChecked(this.automovil.getDisponibilidadPorDia()[DiaDeLaSemana.MARTES.ordinal()]);
            togMiercoles.setChecked(this.automovil.getDisponibilidadPorDia()[DiaDeLaSemana.MIERCOLES.ordinal()]);
            togJueves.setChecked(this.automovil.getDisponibilidadPorDia()[DiaDeLaSemana.JUEVES.ordinal()]);
            togViernes.setChecked(this.automovil.getDisponibilidadPorDia()[DiaDeLaSemana.VIERNES.ordinal()]);
            togSabado.setChecked(this.automovil.getDisponibilidadPorDia()[DiaDeLaSemana.SABADO.ordinal()]);
        } else {
            cbAuto.setChecked(false);
            etModeloAuto.setEnabled(false);
            spnAnio.setEnabled(false);
            spnCapacidad.setEnabled(false);
            togLunes.setEnabled(false);
            togMartes.setEnabled(false);
            togMiercoles.setEnabled(false);
            togJueves.setEnabled(false);
            togViernes.setEnabled(false);
            togSabado.setEnabled(false);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Disponiblidad de Auto")
                .setView(dialogView)
                .setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (cbAuto.isChecked()) {
                            automovil.setModelo(etModeloAuto.getText().toString());
                            automovil.setAnio(Integer.parseInt(spnAnio.getSelectedItem().toString()));
                            automovil.setCapacidad(Integer.parseInt(spnCapacidad.getSelectedItem().toString()));
                            Boolean[] disponibilidadPorDia = {togLunes.isChecked(),
                                    togMartes.isChecked(), togMiercoles.isChecked(),
                                    togJueves.isChecked(), togViernes.isChecked(),
                                    togSabado.isChecked()};
                            automovil.setDisponibilidadPorDia(disponibilidadPorDia);
                        }

                        raListener.checarDatosAuto();
                    }
                });

        cbAuto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                etModeloAuto.setEnabled(isChecked);
                spnAnio.setEnabled(isChecked);
                spnCapacidad.setEnabled(isChecked);
                togLunes.setEnabled(isChecked);
                togMartes.setEnabled(isChecked);
                togMiercoles.setEnabled(isChecked);
                togJueves.setEnabled(isChecked);
                togViernes.setEnabled(isChecked);
                togSabado.setEnabled(isChecked);

                if (!isChecked) {
                    automovil.clear();
                    togLunes.setChecked(false);
                    togMartes.setChecked(false);
                    togMiercoles.setChecked(false);
                    togJueves.setChecked(false);
                    togViernes.setChecked(false);
                    togSabado.setChecked(false);
                }
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            this.raListener = (RegistroAutoListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +  " must implement RegistroAutoListener");
        }
    }

    private int indexOfAnio(String anio) {
        for (int i = 0; i < ANIOS.length; i++) {
            if (ANIOS[i].equals(anio))
                return i;
        }

        return -1;
    }

    private int indexOfCapacidad(String capacidad) {
        for (int i = 0; i < CAPACIDADES.length; i++) {
            if (CAPACIDADES[i].equals(capacidad))
                return i;
        }

        return -1;
    }

    public interface RegistroAutoListener {
        boolean checarDatosAuto();
    }
}
