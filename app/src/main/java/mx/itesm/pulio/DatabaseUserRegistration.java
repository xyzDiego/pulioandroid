package mx.itesm.pulio;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DatabaseUserRegistration {

    private final CollectionReference organizacionesRef = FirebaseFirestore.getInstance().collection("organizacion");
    private final CollectionReference usuariosRef = FirebaseFirestore.getInstance().collection("usuario");
    private final DocumentReference indicesRef = FirebaseFirestore.getInstance().collection("indices").document("ultimoIndice");

    private UserRegistrationListener urli;
    private FirebaseAuth firebaseAuthentication;
    private Map<String, Object> nuevoUsuario;
    private int indiceUsuario;
    private Organizacion organizacionUsuario;

    public ArrayList<Organizacion> organizaciones;

    public DatabaseUserRegistration(DatabaseUserRegistration.UserRegistrationListener urli) {
        this.urli = urli;
        this.firebaseAuthentication = FirebaseAuth.getInstance();
        this.nuevoUsuario = new HashMap<>();
        this.organizaciones = new ArrayList<>();

        organizacionesRef.get().addOnCompleteListener(new OrganizationsRetrieval());
        indicesRef.get().addOnCompleteListener(new UserIndexUpdate());
    }

    //Verifica que el correo no esté ya registrado
    public void verificarCorreo(String correo) {
        usuariosRef.whereEqualTo("correo", correo)
                .get()
                .addOnSuccessListener(new MailVerification());
    }

    //Registra un nuevo usuario a la base de datos
    public void registrarUsuario(String nombre, String correo, String contrasena, String numero, String nombreInstitucion) {
        this.nuevoUsuario.put("nombre", nombre);
        this.nuevoUsuario.put("correo", correo.toLowerCase());
        this.nuevoUsuario.put("num_tel", Long.parseLong(numero));

        for (Organizacion organizacion: this.organizaciones) {
            if (organizacion.getNombre().equals(nombreInstitucion)) {
                this.nuevoUsuario.put("organizacionId", organizacion.getId());
                organizacionUsuario = organizacion;
                break;
            }
        }

        firebaseAuthentication.createUserWithEmailAndPassword(correo, contrasena).
                addOnCompleteListener(new UserRegistration());
    }

    //Consulta todas las organizaciones de la colección organización
    private class OrganizationsRetrieval implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            for (QueryDocumentSnapshot document : task.getResult()) {
                if (document.contains("nombre") && document.contains("dominio")) {
                    Lugar lugar = new Lugar("oficina",
                            document.get("direccion", String.class),
                            document.get("latitud", Double.class),
                            document.get("longitud", Double.class));
                    Organizacion organizacion = new Organizacion(document.getId(),
                            document.get("nombre", String.class),
                            document.get("dominio", String.class),
                            lugar);
                    organizaciones.add(organizacion);
                }
                else {
                    Log.e("DUR",
                            "los campos dominio y nombre no se encontraron para la organización " +
                                    document.getId());
                }
            }
        }

    }

    //Actualiza índice del último usuario creado
    private class UserIndexUpdate implements OnCompleteListener<DocumentSnapshot> {

        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            DocumentSnapshot document = task.getResult();

            indiceUsuario = Integer.parseInt(document.getData().get("usuario").toString()) + 1;
            indicesRef.update("usuario", indiceUsuario);
        }
    }

    private class MailVerification implements OnSuccessListener {

        @Override
        public void onSuccess(Object object) {
            QuerySnapshot querySnapshot = (QuerySnapshot) object;
            //Un query snap con cero documentos indica que no se encontraron correos iguales
            urli.onMailVerified(querySnapshot.getDocuments().isEmpty());
        }
    }

    private class UserRegistration implements OnCompleteListener<AuthResult> {

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful() && indiceUsuario != 0) {
                usuariosRef.document("u" + indiceUsuario).set(nuevoUsuario).
                        addOnSuccessListener(new RegisterVerification());
            } else {
                Log.w("DUR", "could not create new user in firebase", task.getException());
                urli.onRegisterCompleted(false);
            }
        }
    }

    private class RegisterVerification implements OnSuccessListener {

        @Override
        public void onSuccess(Object o) {
            DataHolder.getInstance().setUsuario(new Usuario(
                    "u" + indiceUsuario,
                    "" + nuevoUsuario.get("nombre"),
                    "" + nuevoUsuario.get("correo"),
                    organizacionUsuario,
                    Long.parseLong("" + nuevoUsuario.get("num_tel"))));
            urli.onRegisterCompleted(true);
        }
    }

    public interface UserRegistrationListener {
        void onMailVerified(boolean mailValido);
        void onRegisterCompleted(boolean registroExitoso);
    }

}
