package mx.itesm.pulio;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.chad.pulioapp.R;

public class LoginActiv extends AppCompatActivity implements DatabaseLogin.LoginListener {

    private TextView tvCorreoErrorLogin;
    private TextView tvContraErrorLogin;
    private EditText eTCorreoLogin;
    private EditText eTContraLogin;
    private ProgressDialog progressDialog;

    private DatabaseLogin dbLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tvCorreoErrorLogin = findViewById(R.id.tvCorreoErrorLogin);
        tvContraErrorLogin = findViewById(R.id.tvContraErrorLogin);

        eTCorreoLogin = findViewById(R.id.eTCorreoLogin);
        eTCorreoLogin.requestFocus();
        eTContraLogin = findViewById(R.id.eTContraLogin);

        this.overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);

        tvCorreoErrorLogin.setTextSize(0f);
        tvContraErrorLogin.setTextSize(0f);

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Iniciando sesión...");

        this.dbLogin = new DatabaseLogin(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void siguiente(View v) {
        if (checarDatos()) {
            progressDialog.show();
            autenticarUsuario();
        }
    }

    private boolean checarDatos() {
        boolean datosValidos = true;
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        //Checar Correo
        if (eTCorreoLogin.getText().toString().isEmpty()) {
            llamarError(eTCorreoLogin, tvCorreoErrorLogin, "*Campo requerido", shake);
            datosValidos = false;
        } else if (!eTCorreoLogin.getText().toString().matches("^[\\w\\.-]+@([\\w\\-]+\\.)+[a-z]{2,4}$")) {
            llamarError(eTCorreoLogin, tvCorreoErrorLogin, "*Ingresar un correo válido", shake);
            datosValidos = false;
        } else {
            resetearError(tvCorreoErrorLogin);
        }

        //Checar Contrasena
        if (eTContraLogin.getText().toString().isEmpty()) {
            llamarError(eTContraLogin, tvContraErrorLogin, "*Campo requerido", shake);
            datosValidos = false;
        } else if (eTContraLogin.getText().toString().length() < 8) {
            llamarError(eTContraLogin, tvContraErrorLogin, "*Contraseña incorrecta", shake);
            datosValidos = false;
        } else if (!eTContraLogin.getText().toString().matches(".*\\d+.*")) {
            llamarError(eTContraLogin, tvContraErrorLogin, "*Contraseña incorrecta", shake);
            datosValidos = false;
        } else {
            resetearError(tvContraErrorLogin);
        }

        return datosValidos;
    }

    private void llamarError(EditText et, TextView tv, String error, Animation a) {
        et.startAnimation(a);
        tv.setText(error);
        tv.setTextColor(Color.RED);
        tv.setTextSize(10f);
    }

    private void resetearError(TextView tv) {
        tv.setText("");
        tv.setTextSize(0f);
    }

    private void autenticarUsuario() {
        dbLogin.autenticarUsuario(eTCorreoLogin.getText().toString(), eTContraLogin.getText().toString());
    }

    @Override
    public void onLoginCompleted(boolean autenticacionExitosa) {
        progressDialog.dismiss();
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        if (autenticacionExitosa) {
            Intent intent = new Intent(this, MisRutasActiv.class);
            startActivity(intent);
        } else {
            llamarError(eTCorreoLogin, tvCorreoErrorLogin, "*Correo o contraseña incorrecta", shake);
            llamarError(eTContraLogin, tvContraErrorLogin, "*Correo o contraseña incorrecta", shake);
        }
    }

    public void restaurarContrasena(View v){
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
        String correo = eTCorreoLogin.getText().toString();

        if (correo.isEmpty() || !correo.matches("^[\\w\\.-]+@([\\w\\-]+\\.)+[a-z]{2,4}$")) {
            llamarError(eTCorreoLogin, tvCorreoErrorLogin, "*Ingresa un correo para restaurar tu contraseña", shake);
        } else {

            resetearError(tvCorreoErrorLogin);

            new AlertDialog.Builder(this)
                    .setTitle("Restaura mi contraseña")
                    .setCancelable(false)
                    .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dbLogin.restaurarContrasena(correo);

                        }
                    })
                    .show();
        }

        resetearError(tvContraErrorLogin);
    }

    @Override
    public void onResetPasswordCompleted(boolean restauracionExitosa) {
        if (restauracionExitosa) {
            new AlertDialog.Builder(this).setMessage("Se ha enviado a tu correo una liga para restaurar tu contraseña").
                    setNeutralButton("Entendido", null).show();
        } else {
            new AlertDialog.Builder(this).setMessage("Asegura que el correo que ingresaste para restaurar tu contraseña es válido").
                    setNeutralButton("Entendido", null).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);
    }
}
