package mx.itesm.pulio;

import android.animation.Animator;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.chad.pulioapp.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

public class InicioActiv extends AppCompatActivity{

    private static final String TAG = "InicioActiv";

    ImageView ivPulioInicio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivPulioInicio = findViewById(R.id.ivPulioInicio);
        animateLogo();

        ivPulioInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateLogo();
            }
        });
    }

    public void toRegistro(View v){
        Intent intent = new Intent(this, RegistroActiv.class);
        startActivity(intent);
    }

    public void toLogin(View v){
        Intent intent = new Intent(this, LoginActiv.class);
        startActivity(intent);
    }

    public void animateLogo() {
        YoYo.with(Techniques.FlipOutY)
                .delay(600)
                .duration(600)
                .pivot(170f+(170/2f),205f+(205/2f))
                .interpolate(new AccelerateDecelerateInterpolator())
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ivPulioInicio.setImageResource(R.drawable.pulio2logo);
                        YoYo.with(Techniques.FlipInY)
                                .duration(600)
                                .pivot(170f+(170/2f),205f+(205/2f))
                                .interpolate(new AccelerateDecelerateInterpolator())
                                .playOn(ivPulioInicio);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(ivPulioInicio);
    }
}

