package mx.itesm.pulio;

public enum StatusRequest {

    OK,
    //Errores arrojados por la consulta a la api de google
    ZERO_RESULTS,
    OVER_QUERY_LIMIT,
    REQUEST_DENIED,
    INVALID_REQUEST,
    UNKNOWN_ERROR,
    //Errores arrojados por la ejecución de los threads
    MALFORMED_URL,
    IO_EXCEPTION,
    UNKNOWN_EXCEPTION
}
