package mx.itesm.pulio;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class DatabaseNotificationSender {

    private static final String TAG = "DBNotificationSender";

    //Este método le envía al chofer la notificación de que el usuario se quiere inscribir a su ruta
    public void sendNotificacionRequest(DatabaseNotificationSenderListener databaseNotificationSenderli,
                                        Ruta ruta, int numParada) {
        String idRemitente = DataHolder.getInstance().getUsuario().getId();
        String idDestinatario = ruta.getChofer().getId();

        DocumentReference notificacionDocumentReference = FirebaseFirestore.getInstance().
                collection("usuario").document(ruta.getChofer().getId()).
                collection("mailbox").document(idRemitente + idDestinatario + ruta.getId() + "r");

        Map<String, Object> map = new HashMap<>();
        map.put("idRemitente", idRemitente);
        map.put("idDestinatario", idDestinatario);
        map.put("idRuta", "r" + ruta.getId());
        map.put("isRequest", true);
        map.put("numParada", numParada);
        map.put("lugarDireccion", ruta.getParadas().get(numParada).getLugar().getDireccion());
        map.put("lugarId", ruta.getParadas().get(numParada).getLugar().getId());
        map.put("lugarLatitud", ruta.getParadas().get(numParada).getLugar().getLatitud());
        map.put("lugarLongitud", ruta.getParadas().get(numParada).getLugar().getLongitud());
        map.put("pasajeroNombre", ruta.getParadas().get(numParada).getPasajero().getNombre());
        map.put("pasajeroTelefono", ruta.getParadas().get(numParada).getPasajero().getTelefono());
        map.put("idPasajero",ruta.getParadas().get(numParada).getPasajero().getIdUsuario());

        notificacionDocumentReference.set(map).addOnCompleteListener(new NotificacionSend(databaseNotificationSenderli, ruta));
    }

    //Este método le envía al passajera la notificación de que el chofer a aceptado o declinado
    //su solicitud
    public void sendNotificacionAcknowledege(DatabaseNotificationSenderListener databaseNotificationSenderli,
                                             NotificacionRequest notificacionRequest, boolean aceptado) {
        String idRemitente = DataHolder.getInstance().getUsuario().getId();
        String idDestinatario = notificacionRequest.getIdRemitente();

        DocumentReference notificacionDocumentReference = FirebaseFirestore.getInstance().
                collection("usuario").document(idDestinatario).
                collection("mailbox").document(idRemitente +
                idDestinatario + notificacionRequest.getRuta().getId() + "a");

        Map<String, Object> map = new HashMap<>();
        map.put("isRequest",false);
        map.put("idRemitente", idRemitente);
        map.put("idDestinatario", idDestinatario);
        map.put("idRuta","r"+notificacionRequest.getRuta().getId());
        map.put("choferNombre", notificacionRequest.getRuta().getChofer().getNombre());
        map.put("dia", notificacionRequest.getRuta().getDia().name());
        map.put("hora", notificacionRequest.getRuta().getParadas().get(notificacionRequest.getNumParada()).getHora());
        map.put("minuto", notificacionRequest.getRuta().getParadas().get(notificacionRequest.getNumParada()).getMinuto());
        map.put("aceptado", aceptado);

        notificacionDocumentReference.set(map).addOnCompleteListener(
                new NotificacionSend(databaseNotificationSenderli, notificacionRequest.getRuta()));
    }

    private class NotificacionSend implements OnCompleteListener<Void> {

        private final DatabaseNotificationSenderListener databaseNotificationSenderli;
        private final Ruta ruta;

        public NotificacionSend(DatabaseNotificationSenderListener databaseNotificationSenderli, Ruta ruta) {
            this.databaseNotificationSenderli = databaseNotificationSenderli;
            this.ruta = ruta;
        }

        @Override
        public void onComplete(@NonNull Task<Void> task) {
            if (task.isSuccessful()) {
                Log.d(TAG, "Notificacion a usuario exitosa");
                databaseNotificationSenderli.onNotificationSend(true, ruta);
            } else {
                Log.d(TAG, "Notificacion a usuario fallida");
                databaseNotificationSenderli.onNotificationSend(false, ruta);
            }
        }

    }

    public interface DatabaseNotificationSenderListener {
        void onNotificationSend(boolean operacionExitosa, Ruta ruta);
    }
}
