package mx.itesm.pulio;

import android.util.Log;

public class CreadorRutas {

    private static final String TAG = "CreadorRutas";

    private Usuario usuario = DataHolder.getInstance().getUsuario();
    private CreacionRutasListener creacionRutasli;
    private boolean creacionExitosa;

    public CreadorRutas(CreacionRutasListener creacionRutasli) {
        this.creacionRutasli = creacionRutasli;
        this.creacionExitosa = true;
    }

    //Este método se invoca cuando el usuario registró por primera vez sus horarios, lugares y
    //automóvil y es llamado desde usuario.crearMisRutas().
    //Para este punto los arreglos rutasLlegada y rutasSalida del usuario están llenos con todos los
    //objetos ruta, aunque sus arreglos de paradas están vacíos.
    //Recordar que se construyeron dos objetos ruta (uno de llegada y otro de salida) por cada día
    //que el usuario registró un automóvil.
    //De este modo, la función de este método es completar la construcción de los objetos ruta
    //agregando las únicas dos paradas existentes al momento que son el punto de partida y el punto
    //de destino. Para una ruta de llegada el punto de partida es el lugar desde donde se traslada
    //el usuario y el punto de destino es la oficina, mientras que para una ruta de salida
    //es al revés.
    //El proceso consta de los siguientes pasos:
    //1. Se construyen las paradas de las rutas de llegada y salida con la información que ya se
    //   tiene. El resto de la información necesaria para poder completar la construcción de los
    //   objetos paradas se tiene que recolectar a través de consultas al api de google maps,
    //   por ello se crean objetos de tipo TiempoPartidaQuery y TiempoSalidaQuery. Cada
    //   uno de estos objetos es un thread que al ejecutarse hará la respectiva consulta.
    //2. Se hacen las consultas al api de google iniciando los threads con thread.start()
    //3. Se espera a que las consultas hayan acabado con la operación thread.join()
    //4. Se terminan de constuir los objetos paradas de las rutas de llegada y salida utilizando
    //   los datos que hayan regresado las consultas
    //5. Se le avisa al listener si la construcción de todas las rutas fue exitosa o no.
    public void crearRutas() {
        Thread[] threadPoolQueriesLlegada = new Thread[usuario.getRutasLlegada().size()];
        Thread[] threadPoolQueriesSalida = new Thread[usuario.getRutasSalida().size()];

        for (int i = 0; i < usuario.getRutasLlegada().size(); i++) {
            inicializarParadasRutaLlegada(i, threadPoolQueriesLlegada);
        }

        for (int i = 0; i < usuario.getRutasSalida().size(); i++) {
            inicializarParadasRutaSalida(i, threadPoolQueriesSalida);
        }

        for (int i = 0; i < threadPoolQueriesLlegada.length; i++) {
            threadPoolQueriesLlegada[i].start();
            threadPoolQueriesSalida[i].start();
        }

        for (int i = 0; i < threadPoolQueriesLlegada.length; i++) {
            try {
                threadPoolQueriesLlegada[i].join();
                threadPoolQueriesSalida[i].join();
            } catch (InterruptedException e) {
                creacionExitosa = false;
                Log.e(TAG, "Algún thread lanzó una excepción " + e.getMessage());
            }
        }

        for (int i = 0; i < usuario.getRutasLlegada().size(); i++) {
            terminarCreacionRutaLlegada(i, threadPoolQueriesLlegada);
        }

        for (int i = 0; i < usuario.getRutasSalida().size(); i++) {
            terminarCreacionRutaSalida(i, threadPoolQueriesSalida);
        }

        if (!this.creacionExitosa) {
            //Se restauran los arreglos al estado en que estaban antes de iniciar con el proceso
            //de creación de rutas
            usuario.getRutasLlegada().clear();
            usuario.getRutasSalida().clear();
        }

        this.creacionRutasli.onRutasCreadas(this.creacionExitosa);
    }

    private void inicializarParadasRutaLlegada(int i, Thread[] threadPoolQueriesLlegada) {
        Ruta ruta = usuario.getRutasLlegada().get(i);

        Parada paradaOrigen = new Parada();
        Parada paradaDestino = new Parada();

        ruta.getParadas().add(0, paradaOrigen);
        ruta.getParadas().add(ruta.getParadas().size(), paradaDestino);

        paradaOrigen.setLugar(usuario.getHorariosLlegada()[ruta.getDia().ordinal()].getLugar());
        paradaOrigen.setPasajero(new Pasajero(usuario.getId(), usuario.getNombre(), usuario.getNum_tel()));
        paradaOrigen.setDistanciaKm(0);

        paradaDestino.setLugar(usuario.getOrganizacion().getLugar());
        paradaDestino.setPasajero(null);
        paradaDestino.setHora(usuario.getHorariosLlegada()[ruta.getDia().ordinal()].getHora());
        paradaDestino.setMinuto(usuario.getHorariosLlegada()[ruta.getDia().ordinal()].getMinuto());

        //Para obtener el resto de los parametros de las paradas se hará un query a la api de google
        double origenLatitud = paradaOrigen.getLugar().getLatitud();
        double origenLongitud = paradaOrigen.getLugar().getLongitud();
        double destinoLatitud = paradaDestino.getLugar().getLatitud();
        double destinoLongitud = paradaDestino.getLugar().getLongitud();

        TiempoPartidaQuery tiempoPartidaQuery = new TiempoPartidaQuery(
                origenLatitud, origenLongitud,
                destinoLatitud, destinoLongitud,
                paradaDestino.getHora(), paradaDestino.getMinuto(),
                ruta.getDia());
        threadPoolQueriesLlegada[i] = tiempoPartidaQuery;
    }

    private void inicializarParadasRutaSalida(int i, Thread[] threadPoolQueriesSalida) {
        Ruta ruta = usuario.getRutasSalida().get(i);

        Parada paradaOrigen = new Parada();
        Parada paradaDestino = new Parada();
        ruta.getParadas().add(0, paradaOrigen);
        ruta.getParadas().add(ruta.getParadas().size(), paradaDestino);

        paradaOrigen.setLugar(usuario.getOrganizacion().getLugar());
        paradaOrigen.setPasajero(null);
        paradaOrigen.setHora(usuario.getHorariosSalida()[ruta.getDia().ordinal()].getHora());
        paradaOrigen.setMinuto(usuario.getHorariosSalida()[ruta.getDia().ordinal()].getMinuto());
        paradaOrigen.setDistanciaKm(0);

        paradaDestino.setLugar(usuario.getHorariosSalida()[ruta.getDia().ordinal()].getLugar());
        paradaDestino.setPasajero(new Pasajero(usuario.getId(), usuario.getNombre(), usuario.getNum_tel()));

        //Para obtener el resto de los parametros de las paradas se hará un query a la api de google
        double origenLatitud = paradaOrigen.getLugar().getLatitud();
        double origenLongitud = paradaOrigen.getLugar().getLongitud();
        double destinoLatitud = paradaDestino.getLugar().getLatitud();
        double destinoLongitud = paradaDestino.getLugar().getLongitud();

        TiempoSalidaQuery tiempoSalidaQuery = new TiempoSalidaQuery(
                origenLatitud, origenLongitud, destinoLatitud, destinoLongitud,
                paradaOrigen.getHora(), paradaOrigen.getMinuto(), ruta.getDia());
        threadPoolQueriesSalida[i] = tiempoSalidaQuery;
    }

    private void terminarCreacionRutaLlegada(int i, Thread[] threadPoolQueriesLlegada) {
        TiempoPartidaQuery tiempoLlegadaQuery = (TiempoPartidaQuery) threadPoolQueriesLlegada[i];
        Ruta ruta = usuario.getRutasLlegada().get(i);

        Parada paradaOrigen = ruta.getParadas().get(0);
        Parada paradaDestino = ruta.getParadas().get(ruta.getParadas().size() - 1);

        if (tiempoLlegadaQuery.getStatus() == StatusRequest.OK) {
            paradaOrigen.setHora(tiempoLlegadaQuery.getOrigenHora());
            paradaOrigen.setMinuto(tiempoLlegadaQuery.getOrigenMinuto());

            paradaDestino.setDistanciaKm(tiempoLlegadaQuery.getDistanciaKm());
        } else {
            Log.e(TAG, "La construccion de la ruta " + ruta.toString() + " fallo;" +
                    "tiempoLlegadaQuery" + tiempoLlegadaQuery.toString());
            this.creacionExitosa = false;
        }
    }

    private void terminarCreacionRutaSalida(int i, Thread[] threadPoolQueriesSalida) {
        TiempoSalidaQuery tiempoSalidaQuery = (TiempoSalidaQuery) threadPoolQueriesSalida[i];
        Ruta ruta = usuario.getRutasSalida().get(i);

        Parada paradaDestino = ruta.getParadas().get(ruta.getParadas().size() - 1);

        if (tiempoSalidaQuery.getStatus() == StatusRequest.OK) {
            paradaDestino.setHora(tiempoSalidaQuery.getDestinoHora());
            paradaDestino.setMinuto(tiempoSalidaQuery.getDestinoMinuto());
            paradaDestino.setDistanciaKm(tiempoSalidaQuery.getDistanciaKm());
        } else {
            Log.e(TAG, "La construccion de la ruta " + ruta.toString() + " fallo;" +
                    "tiempoLlegadaQuery" + tiempoSalidaQuery.toString());
            this.creacionExitosa = false;
        }
    }

    public interface CreacionRutasListener {
        void onRutasCreadas(boolean creacionExitosa);
    }
}
