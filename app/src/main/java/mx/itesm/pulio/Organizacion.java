package mx.itesm.pulio;

import android.util.Log;

import java.util.LinkedList;

public class Organizacion {

    private static final String TAG = "Organizacion";
    private final String id;
    private final String nombre;
    private final String dominio;
    private final Lugar lugar;
    private final LinkedList<Ruta> rutas;

    public Organizacion(String id, String nombre, String dominio, Lugar lugar) {
        this.id = id;
        this.nombre = nombre;
        this.dominio = dominio;
        this.lugar = lugar;
        this.rutas = new LinkedList<>();
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDominio() {
        return dominio;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public LinkedList<Ruta> getRutas() {
        return rutas;
    }

    public Ruta buscarRuta(String id){
        String rutaId;
        for(Ruta ruta : DataHolder.getInstance().getUsuario().getOrganizacion().getRutas()){
            rutaId = "r" + ruta.getId();
            if(rutaId.equals(id)){
                return ruta;
            }
        }
        Log.d(TAG, "No se encontró la ruta");
        return null;
    }

    @Override
    public String toString() {
        return "Organizacion{" +
                "id=" + id + "\n" +
                "nombre=" + nombre + "\n" +
                "dominio=" + dominio + "\n" +
                "lugar=" + lugar.toString() + "\n" +
                '}';
    }
}
