package mx.itesm.pulio;

import android.util.Log;

import com.google.common.base.Predicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class  Usuario {

    private static final String TAG = "Usuario";

    private final String id;
    private final String nombre;
    private final String correo;
    private Organizacion organizacion;
    private final long num_tel;

    private final Automovil automovil;
    private final ArrayList<Lugar> lugares;
    //Horarios de Lunes a Sábado en los que el usuario parte (horariosLlegada) hacia el trabajo
    //y en los que se regresa del trabajo (horariosSalida).
    //En caso de que el usuario no labore el día i, entonces horario[i] apuntará a null
    private final Horario[] horariosLlegada;
    private final Horario[] horariosSalida;
    private final ArrayList<Ruta> rutasLlegada;
    private final ArrayList<Ruta> rutasSalida;
    private final LinkedList<Ruta> rutasCandidatas;
    private final Mailbox mailbox;

    public Usuario(String id, String nombre, String correo, Organizacion organizacion, long num_tel) {
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.organizacion = organizacion;
        this.num_tel = num_tel;

        this.automovil = new Automovil("a1");
        this.lugares = new ArrayList<>();
        this.horariosLlegada = new Horario[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
        this.horariosSalida = new Horario[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
        this.rutasLlegada = new ArrayList<>();
        this.rutasSalida = new ArrayList<>();
        this.rutasCandidatas = new LinkedList<>();
        this.mailbox = new Mailbox(id);
    }

    public Usuario(String id, String nombre, String correo, long num_tel){
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.num_tel = num_tel;

        this.automovil = new Automovil("a1");
        this.lugares = new ArrayList<>();
        this.horariosLlegada = new Horario[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
        this.horariosSalida = new Horario[DiaDeLaSemana.TOTAL_DIAS.ordinal()];
        this.rutasLlegada = new ArrayList<>();
        this.rutasSalida = new ArrayList<>();
        this.rutasCandidatas = new LinkedList<>();
        this.mailbox = new Mailbox(id);
    }

    public String getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public Organizacion getOrganizacion() {
        return organizacion;
    }

    public long getNum_tel() {
        return num_tel;
    }

    public Automovil getAutomovil() {
        return automovil;
    }

    public ArrayList<Lugar> getLugares() {
        return lugares;
    }

    public Horario[] getHorariosLlegada() {
        return horariosLlegada;
    }

    public Horario[] getHorariosSalida() {
        return horariosSalida;
    }

    public ArrayList<Ruta> getRutasLlegada() {
        return rutasLlegada;
    }

    public ArrayList<Ruta> getRutasSalida() {
        return rutasSalida;
    }

    public LinkedList<Ruta> getRutasCandidatas() {
        return rutasCandidatas;
    }

    public Mailbox getMailbox() {
        return mailbox;
    }

    public void setOrganizacion(Organizacion organizacion) {
        this.organizacion = organizacion;
    }

    @Override
    public String toString() {
        return "Usuario{" + "\n" +
                "id=" + id + "\n"+
                "nombre=" + nombre + "\n" +
                "correo=" + correo + "\n" +
                "organizacion=" + organizacion + "\n" +
                "num_tel=" + num_tel + "\n" +
                "automovil=" + automovil + "\n" +
                "lugares=" + lugares + "\n" +
                "horariosLlegada=" + Arrays.toString(horariosLlegada) + "\n" +
                "horariosSalida=" + Arrays.toString(horariosSalida) + "\n" +
                "rutasLlegada=" + rutasLlegada + "\n" +
                "rutasSalida=" + rutasSalida + "\n" +
                "rutasCandidatas=" + rutasCandidatas + "\n" +
                "mailbox=" + mailbox.toString() + "\n" +
                '}';
    }

    public void prettyRutasCandidatasString(String tag){
        String s = "Rutas Candidatas:" + "\n" +
                rutasCandidatas + "\n";
        int length = s.length();
        Log.d(tag, s.substring(0, length/10));
        Log.d(tag, s.substring((length/10), (length/10) * 2));
        Log.d(tag, s.substring(2 * (length/10), (length/10) * 3));
        Log.d(tag, s.substring(3 * (length/10), (length/10) * 4));
        Log.d(tag, s.substring(4 * (length/10), (length/10) * 5));
        Log.d(tag, s.substring(5 * (length/10), (length/10) * 6));
        Log.d(tag, s.substring(6 * (length/10), (length/10) * 7));
        Log.d(tag, s.substring(7 * (length/10), (length/10) * 8));
        Log.d(tag, s.substring(8 * (length/10), (length/10) * 9));
        Log.d(tag, s.substring(9 * (length/10), length));
    }

    public void prettyLogString(String tag){
        String s = this.toString();
        int length = s.length();
        Log.d(tag, s.substring(0, length/10));
        Log.d(tag, s.substring((length/10), (length/10) * 2));
        Log.d(tag, s.substring(2 * (length/10), (length/10) * 3));
        Log.d(tag, s.substring(3 * (length/10), (length/10) * 4));
        Log.d(tag, s.substring(4 * (length/10), (length/10) * 5));
        Log.d(tag, s.substring(5 * (length/10), (length/10) * 6));
        Log.d(tag, s.substring(6 * (length/10), (length/10) * 7));
        Log.d(tag, s.substring(7 * (length/10), (length/10) * 8));
        Log.d(tag, s.substring(8 * (length/10), (length/10) * 9));
        Log.d(tag, s.substring(9 * (length/10), length));
    }

    public Lugar searchLugar(String idLugar) {
        for (Lugar lugar : lugares) {
            if (lugar.getId().equals(idLugar))
                return lugar;
        }
        return null;
    }

    //Este método se invoca cuando el usuario registró por primera vez sus horarios, lugares y
    //automóvil. Por cada día que el usuario use un automóvil se creará una nueva ruta.
    public void crearMisRutas(CreadorRutas.CreacionRutasListener creacionRutasli) {
        Usuario usuario = DataHolder.getInstance().getUsuario();

        if (!usuario.getAutomovil().esUtilizado()) {
            creacionRutasli.onRutasCreadas(true);
        } else {
            Chofer chofer = new Chofer(usuario.getId(),
                                        usuario.getNombre(),
                                        usuario.getNum_tel(),
                                        usuario.getAutomovil().getCapacidad());

            for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
                if (usuario.getAutomovil().getDisponibilidadPorDia()[dia]) {
                    Ruta rutaLlegada = new Ruta(DiaDeLaSemana.values()[dia], true, chofer);
                    Ruta rutaSalida = new Ruta(DiaDeLaSemana.values()[dia], false, chofer);
                    usuario.rutasLlegada.add(rutaLlegada);
                    usuario.rutasSalida.add(rutaSalida);
                }
            }

            CreadorRutas creadorRutas = new CreadorRutas(creacionRutasli);
            creadorRutas.crearRutas();
        }
    }

    //Proceso de construir el arreglo de rutas candidatas
    //1. Se copian todas las rutas de organizacion.rutas al arreglo de rutasCandidatas del usuario
    //2. Se eliminan del arreglo de rutasCandidatas todas las rutas que cumplan con alguna de las siguientes condiciones:
    //  a) el número de inscritos iguala la capacidad total del auto.
    //  b) el valor día de la ruta equivale a un día en el que el usuario no labora.
    //  c) la ruta es de llegada y el horario de la última parada es mayor al horario de llegada del usuario para ese día
    //  d) la ruta es de salida y el horario de la primera parada es menor al horario de salida del usuario para ese día
    //  e-f) el usuario ya tiene inscrita una ruta para ese día y para esa dirección
    //3. A todas las rutas restantes en el arreglo de rutasCandidatas se les agregar como parada intermedia
    //   una nueva parada cuyo lugar es la ubicación desde o hacia que el usuario viaja para ese día
    //   y cuyo pasajero es igual al usuario
    //4. Para todas las rutas del arreglo rutasCandidatas se calcula la ruta óptima en la cual
    //   se reordenan las paradas
    //5. Para todas las nuevas rutas óptimas se recalcula distancias y tiempos de llegada de cada
    //   parada
    public void construirRutasCandidatas(ConstruccionRutasCandidatasListener construccionRutasCandidatasli) {
        Boolean operacionExitosa = new Boolean(true);
        Usuario usuario = DataHolder.getInstance().getUsuario();

        //Paso 1
        for (Ruta ruta : this.getOrganizacion().getRutas())
            this.rutasCandidatas.add(new Ruta(ruta));
        //usuario.prettyRutasCandidatasString(TAG);

        //Paso 2
        Predicate<Ruta> condicionA = ruta -> ruta.getInscritos() == ruta.getChofer().getCapacidadAuto();
        Predicate<Ruta> condicionB = ruta -> DataHolder.getInstance().getUsuario().getHorariosLlegada()[ruta.getDia().ordinal()] == null;
        Predicate<Ruta> condicionC = ruta -> {
            if (ruta.getIsLlegada()) {
                Parada ultimaParada = ruta.getParadas().get(ruta.getParadas().size() - 1);
                if (ultimaParada.getMinutosTotales() >
                        usuario.getHorariosLlegada()[ruta.getDia().ordinal()].getMinutosTotales())
                    return true;
            }
            return false;
        };
        Predicate<Ruta> condicionD = ruta -> {
            if (!ruta.getIsLlegada()) {
                Parada primeraParada = ruta.getParadas().get(0);
                if (primeraParada.getMinutosTotales() <
                        usuario.getHorariosSalida()[ruta.getDia().ordinal()].getMinutosTotales())
                    return true;
            }
            return false;
        };
        Predicate<Ruta> condicionE = ruta -> {
            for (Ruta miRuta: this.rutasLlegada) {
                if (ruta.getIsLlegada() && miRuta.getDia() == ruta.getDia())
                    return true;
            }
            return false;
        };
        Predicate<Ruta> condicionF = ruta -> {
            for (Ruta miRuta: this.rutasSalida) {
                if (!ruta.getIsLlegada() && miRuta.getDia() == ruta.getDia())
                    return true;
            }
            return false;
        };

        removerRutas(this.rutasCandidatas, condicionA, condicionB, condicionC, condicionD, condicionE, condicionF);
        //usuario.prettyRutasCandidatasString(TAG);

        //Paso 3
        for (Ruta rutaCandidata: this.rutasCandidatas) {
            Parada nuevaParada = new Parada(
                    (rutaCandidata.getIsLlegada() ? usuario.getHorariosLlegada() : usuario.getHorariosSalida())
                            [rutaCandidata.getDia().ordinal()].getLugar(),
                    new Pasajero(usuario.getId(), usuario.getNombre(), usuario.getNum_tel()));
            rutaCandidata.agregarParadaIntermedia(nuevaParada);
        }
        //usuario.prettyRutasCandidatasString(TAG);

        //Paso 4
        Thread[] threadPoolCalculoRutaOptima = new Thread[this.rutasCandidatas.size()];
        int i = 0;
        for (Ruta ruta: this.rutasCandidatas) {
            threadPoolCalculoRutaOptima[i++] = new CalculoRutaOptimaQuery(ruta);
        }

        for (Thread thread : threadPoolCalculoRutaOptima)
            thread.start();
        try {
            for (Thread thread : threadPoolCalculoRutaOptima)
                thread.join();
        } catch (InterruptedException e) {
            Log.e(TAG, "Algún thread lanzó una excepción " + e.getMessage());
            operacionExitosa = false;
        }
        //usuario.prettyRutasCandidatasString(TAG);

        //Paso 5
        for (Ruta ruta: this.rutasCandidatas) {
            if (ruta.getParadas().size() > 2) {
                if (ruta.getIsLlegada()) {
                    ruta.recalcularDistanciasYTiemposRutaLlegada(operacionExitosa);
                } else {
                    ruta.recalcularDistanciasYTiemposRutaSalida(operacionExitosa);
                }
            }
        }
        //usuario.prettyRutasCandidatasString(TAG);

        construccionRutasCandidatasli.onRutasCandidatasCosntruidas(operacionExitosa);
    }

    private void removerRutas(LinkedList<Ruta> rutas, Predicate<Ruta>... predicates) {
        for (Predicate predicate: predicates) {
            int size = rutas.size();
            for (int i = 0; i < size; i++) {
                Ruta ruta = rutas.remove();
                if (!predicate.apply(ruta)) {
                    rutas.add(ruta);
                }
            }
            Log.d(TAG, predicate.toString() + ""+  DataHolder.getInstance().getUsuario().getRutasCandidatas());
        }

    }

    public interface ConstruccionRutasCandidatasListener {
        void onRutasCandidatasCosntruidas(boolean exito);
    }

}


