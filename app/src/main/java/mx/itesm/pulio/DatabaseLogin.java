package mx.itesm.pulio;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class DatabaseLogin {

    private static final String TAG = "DBLogin";

    private FirebaseAuth firebaseAuthentication;
    private LoginListener logli;
    private String correo;
    private FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();

    // Este constructor se usa para log in normal.
    public DatabaseLogin(LoginListener logli) {
        this.firebaseAuthentication = FirebaseAuth.getInstance();
        this.logli = logli;
    }

    // Este constructor se usa para log in automático.
    public DatabaseLogin(LoginListener logli, String correo){
        this.firebaseAuthentication = FirebaseAuth.getInstance();
        this.logli = logli;
        this.correo = correo;
    }

    // Paso 1:
    // Se autentica el usuario.
    public void autenticarUsuario(String correo, String contrasena) {
        this.correo = correo.toLowerCase();
        this.firebaseAuthentication.signInWithEmailAndPassword(correo, contrasena)
                .addOnCompleteListener(new UserVerification()); // Al terminar de ejecutar manda a llamar la clase que está después.

    }

    public void restaurarContrasena(String correo) {
        this.firebaseAuthentication.sendPasswordResetEmail(correo).addOnCompleteListener(new ResetPassword());
    }

    // Paso 2:
    // Se verifica la existencia del usuario mediante su correo.
    private class UserVerification implements OnCompleteListener<AuthResult> {

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            //logli.onLoginCompleted(task.isSuccessful());

            if(!task.isSuccessful()){
                logli.onLoginCompleted(false);
            }
            else {
                CollectionReference collectionReference = firebaseFirestore.collection("usuario");
                collectionReference.whereEqualTo("correo", correo).get().addOnCompleteListener(new UserRetrieval());
            }
        }
    }

    // Paso 2.5:
    // Este paso solo se llama cuando hace Log In automático.
    public void iniciarDescargaDatosUsuario() {
        CollectionReference collectionReference = firebaseFirestore.collection("usuario");
        collectionReference.whereEqualTo("correo", correo).get().addOnCompleteListener(new UserRetrieval());
    }

    // Paso 3.
    // Se obtiene y mapea a Usuario los datos del usuario en la base de datos como por ejemplo el id, nombre, correo, num_tel.
    private class UserRetrieval implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {

            if (!task.isSuccessful() || task.getResult().isEmpty()){
                Log.d(TAG, "Correo no está en la base de datos");
                logli.onLoginCompleted(false);
            }
            else {

                String id = "";
                String nombre = "";
                String correo = "";
                String dominio = "";
                int indexArroba;
                long num_tel = 0;
                for (QueryDocumentSnapshot document : task.getResult()) {
                    id = document.getId();
                    nombre = document.get("nombre", String.class);
                    correo = document.get("correo", String.class);
                    num_tel = document.get("num_tel", Long.class);
                }

                Usuario usuario = new Usuario(id, nombre, correo, num_tel);
                DataHolder.getInstance().setUsuario(usuario);

                //Log.d(TAG,DataHolder.getInstance().getUsuario().toString());
                indexArroba = correo.indexOf('@');
                dominio = correo.substring(indexArroba);

                CollectionReference collectionReferenceOrganizacion = firebaseFirestore.collection("organizacion");
                collectionReferenceOrganizacion.whereEqualTo("dominio", dominio).get().addOnCompleteListener(new OrganizationRetrieval());

            }
        }
    }

    // Paso 4.
    // Se obtiene y mapea la información de las Organizaciones tales como id, dirección, dominio, nombre, etc.
    private class OrganizationRetrieval implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {

            if(!task.isSuccessful() || task.getResult().isEmpty()) {
                Log.d(TAG, "Organizacion no existe en la base de datos");
                logli.onLoginCompleted(false);
            }
            else {

                String id = "";
                String direccion = "";
                String dominio = "";
                String nombre = "";
                double latitud = 0.0;
                double longitud = 0.0;

                for (QueryDocumentSnapshot document : task.getResult()) {
                    id = document.getId();
                    direccion = document.get("direccion", String.class);
                    dominio = document.get("dominio", String.class);
                    nombre = document.get("nombre", String.class);
                    latitud = document.get("latitud", Double.class);
                    longitud = document.get("longitud", Double.class);
                }

                Lugar lugar = new Lugar("Oficina", direccion, latitud, longitud);
                Organizacion organizacion = new Organizacion(id, nombre, dominio, lugar);
                DataHolder.getInstance().getUsuario().setOrganizacion(organizacion);
                String userId = DataHolder.getInstance().getUsuario().getId();
                DocumentReference usuarioAutoReferencia = firebaseFirestore.getInstance().document("usuario/" + userId + "/automovil/a1");
                usuarioAutoReferencia.get().addOnCompleteListener(new AutomovilRetrieval());
            }
        }
    }

    // Paso 5.
    // Se obtiene y mapea la información del automóvil tales como año, capacidad, modelo, usoPorDia, etc. En caso de no contar con un auto pasa al Paso 6.
    private class AutomovilRetrieval implements OnCompleteListener<DocumentSnapshot>{

        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {

            if(task.getResult().getData() != null) {

                int anio;
                int capacidad;
                String modelo;
                String idUsuario;

                Boolean[] usoPorDia;
                DocumentSnapshot datos = task.getResult();

                anio = Integer.parseInt(datos.getData().get("anio").toString());
                capacidad = Integer.parseInt(datos.getData().get("capacidad").toString());
                modelo = datos.getData().get("modelo").toString();
                usoPorDia = ((List<Boolean>) datos.getData().get("disponibilidadPorDia")).toArray(new Boolean[6]);

                DataHolder.getInstance().getUsuario().getAutomovil().setAnio(anio);
                DataHolder.getInstance().getUsuario().getAutomovil().setCapacidad(capacidad);
                DataHolder.getInstance().getUsuario().getAutomovil().setModelo(modelo);
                DataHolder.getInstance().getUsuario().getAutomovil().setDisponibilidadPorDia(usoPorDia);
                idUsuario = DataHolder.getInstance().getUsuario().getId();

                CollectionReference idLugar = firebaseFirestore.collection("usuario").document(idUsuario).collection("lugares");
                idLugar.get().addOnCompleteListener(new LugaresRetrieval());
            }
            else{
                String idUsuario;
                idUsuario = DataHolder.getInstance().getUsuario().getId();

                CollectionReference idLugar = firebaseFirestore.collection("usuario").document(idUsuario).collection("lugares");
                idLugar.get().addOnCompleteListener(new LugaresRetrieval());
            }
        }
    }

    // Paso 6.
    // Se obtienen y mapean los lugares de la base de datos.
    private class LugaresRetrieval implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (!task.isSuccessful() || task.getResult().isEmpty()){
                Log.d(TAG, "No existen lugares registrados");
                logli.onLoginCompleted(false);
            }
            else {

                String idUsuario;
                ArrayList<Lugar> lugares = DataHolder.getInstance().getUsuario().getLugares();

                for (QueryDocumentSnapshot document : task.getResult()) {
                    Lugar lugar = new Lugar(document.get("id").toString(), document.get("direccion").toString(),
                            Double.parseDouble(document.get("latitud").toString()), Double.parseDouble(document.get("longitud").toString()));
                    lugares.add(lugar);
                }
                idUsuario = DataHolder.getInstance().getUsuario().getId();

                CollectionReference idHorarioLlegada = firebaseFirestore.collection("usuario").document(idUsuario).collection("horariosLlegada");
                idHorarioLlegada.get().addOnCompleteListener(new HorariosLlegadaRetrieval());
            }
        }
    }

    // Paso 7.
    // Se obtiene y mapea los Horarios de llegada de un usuario de la base de datos.
    private class HorariosLlegadaRetrieval implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {

            if (!task.isSuccessful() || task.getResult().isEmpty()) {
                Log.d(TAG, "No existen horarios registrados");
                logli.onLoginCompleted(false);
            }
            else {
                String idHorario;
                String idLugar;
                String idUsuario;
                idUsuario = DataHolder.getInstance().getUsuario().getId();
                for (QueryDocumentSnapshot document : task.getResult()) {
                    for (int i = 0; i < DiaDeLaSemana.TOTAL_DIAS.ordinal(); i++) {
                        idHorario = document.getId();

                        idLugar = document.get("idLugar").toString();
                        if(idHorario.equals(DiaDeLaSemana.values()[i].name())) {
                            Horario horario = new Horario(DataHolder.getInstance().getUsuario().searchLugar(idLugar), Integer.parseInt(document.get("hora").toString()),
                                    Integer.parseInt(document.get("minuto").toString()));
                            DataHolder.getInstance().getUsuario().getHorariosLlegada()[DiaDeLaSemana.values()[i].ordinal()] = horario;

                        }
                    }
                }
                CollectionReference idHorarioSalida = firebaseFirestore.collection("usuario").document(idUsuario).collection("horariosSalida");
                idHorarioSalida.get().addOnCompleteListener(new HorariosSalidaRetrieval());
            }
        }
    }

    // Paso 8.
    // Se obtiene y mapea los Horarios Salida de un usuario de la base de datos.
    private class HorariosSalidaRetrieval implements OnCompleteListener<QuerySnapshot> {

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (!task.isSuccessful() || task.getResult().isEmpty()){
                Log.d(TAG, "No existen horarios de salida");
                logli.onLoginCompleted(false);
            }
            else {
                String idHorario;
                String idLugar;
                String idUsuario;
                String idOrganizacion;
                idUsuario = DataHolder.getInstance().getUsuario().getId();
                idOrganizacion = DataHolder.getInstance().getUsuario().getOrganizacion().getId();
                for (QueryDocumentSnapshot document : task.getResult()) {
                    for (int i = 0; i < DiaDeLaSemana.TOTAL_DIAS.ordinal(); i++) {
                        idHorario = document.getId();

                        idLugar = document.get("idLugar").toString();
                        if(idHorario.equals(DiaDeLaSemana.values()[i].name())) {
                            Horario horario = new Horario(DataHolder.getInstance().getUsuario().searchLugar(idLugar), Integer.parseInt(document.get("hora").toString()),
                                    Integer.parseInt(document.get("minuto").toString()));
                            DataHolder.getInstance().getUsuario().getHorariosSalida()[DiaDeLaSemana.values()[i].ordinal()] = horario;
                        }
                    }
                }
                ManejadorRutas manejadorRutas = new ManejadorRutas();
                manejadorRutas.importarRutas(new RutasOrganizacionRetrieval());
            }
        }
    }

    // Paso 9.
    // Se obtienen todas las rutas existentes de las organizaciones para después mapearlas al usuario si es que este contiene la ruta.
    private class RutasOrganizacionRetrieval implements ManejadorRutas.ManejadorRutasListener {
        String idUsuario = DataHolder.getInstance().getUsuario().getId();
        @Override
        public void onImportacionRutasConcluida(boolean importacionExitosa) {
            if (importacionExitosa) {
                Log.d(TAG, "Importación de rutas exitosa");
                DocumentReference idRutasLlegadas = firebaseFirestore.collection("usuario").document(idUsuario);
                idRutasLlegadas.get().addOnCompleteListener(new RutasLlegadaRetrieval());
            } else {
                Log.d(TAG, "Importación de rutas fallida");
                logli.onLoginCompleted(false);
            }
        }
    }

    // Paso 10.
    // Se mapea al usuario las rutas a las que pertenece.
    private class RutasLlegadaRetrieval implements OnCompleteListener<DocumentSnapshot> {

        String[] rutasLlegada;
        String[] rutasSalida;

        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            DocumentSnapshot datos = task.getResult();
            if (task.getResult().getData() != null) {

                if(datos.getData().get("rutasLlegada") != null) {
                    rutasLlegada = ((List<String>) datos.getData().get("rutasLlegada")).toArray(new String[DiaDeLaSemana.TOTAL_DIAS.ordinal()]);
                }
                if(datos.getData().get("rutasSalida") != null) {
                    rutasSalida = ((List<String>) datos.getData().get("rutasSalida")).toArray(new String[DiaDeLaSemana.TOTAL_DIAS.ordinal()]);
                }

                for (Ruta ruta : DataHolder.getInstance().getUsuario().getOrganizacion().getRutas()) {

                    if(datos.getData().get("rutasLlegada") != null) {
                        for (String rutaLlegada : rutasLlegada) {
                            if (rutaLlegada != null && rutaLlegada.equals("r" + ruta.getId())) {
                                DataHolder.getInstance().getUsuario().getRutasLlegada().add(ruta);
                            }
                        }
                    }

                    if(datos.getData().get("rutasSalida") != null) {
                        for (String rutaSalida : rutasSalida) {
                            if (rutaSalida != null && rutaSalida.equals("r" + ruta.getId())) {
                                DataHolder.getInstance().getUsuario().getRutasSalida().add(ruta);
                            }
                        }
                    }
                }
                DataHolder.getInstance().getUsuario().construirRutasCandidatas(new RutasCandidatasConstruccion());
            }
            else{
                Log.d(TAG, "El usuario no tiene rutas");
                DataHolder.getInstance().getUsuario().construirRutasCandidatas(new RutasCandidatasConstruccion());
            }
        }
    }

    // Paso 11.
    // Construccion arreglo rutas candidatas
    private class RutasCandidatasConstruccion implements Usuario.ConstruccionRutasCandidatasListener {
        @Override
        public void onRutasCandidatasCosntruidas(boolean exito) {
            if (exito) {
                Log.d(TAG, "Construcciona arreglo rutas candidatas exitoso");
                logli.onLoginCompleted(true);
            } else {
                Log.d(TAG, "Construcciona arreglo rutas candidatas fallido");
                logli.onLoginCompleted(false);
            }
        }
    }

    private class ResetPassword implements OnCompleteListener<Void> {
        @Override
        public void onComplete(@NonNull Task<Void> task) {
            logli.onResetPasswordCompleted(task.isSuccessful());
        }
    }

    public interface LoginListener {
        void onLoginCompleted(boolean autenticacionExitosa);
        void onResetPasswordCompleted(boolean restauracionExitosa);
    }
}
