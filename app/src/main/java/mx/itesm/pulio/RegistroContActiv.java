package mx.itesm.pulio;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.TextView;

import com.chad.pulioapp.R;

public class RegistroContActiv extends AppCompatActivity
        implements RegistroAutoDialog.RegistroAutoListener, RegistroHorarioDialog.RegistroHorarioListener,
                   DatabaseUserDataRegistration.UserDataRegistrationListener, CreadorRutas.CreacionRutasListener,
                   ManejadorRutas.ManejadorRutasListener, Usuario.ConstruccionRutasCandidatasListener {

    private static final String TAG = "RegistroContActiv";

    private CardView cvAuto, cvLugares, cvHorario;
    private TextView tvErrorDispAuto, tvErrorLugares, tvErrorHorario;
    private ProgressDialog progressDialog;

    private DatabaseUserDataRegistration dbUserDataRegistration;
    private Usuario usuario;
    private boolean rutasCreadas;
    private CheckBox cbAvisoPrivacidad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_cont);

        this.overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);

        cvAuto = findViewById(R.id.cvAuto);
        cvLugares = findViewById(R.id.cvLugaresSalida);
        cvHorario = findViewById(R.id.cvHorario);

        tvErrorDispAuto = findViewById(R.id.tvErrorDispAuto);
        tvErrorLugares = findViewById(R.id.tvErrorLugares);
        tvErrorHorario = findViewById(R.id.tvErrorHorario);

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Cargando...");

        cbAvisoPrivacidad = findViewById(R.id.cbAvisoPrivacidad);

        this.dbUserDataRegistration = new DatabaseUserDataRegistration(this);
        this.usuario = DataHolder.getInstance().getUsuario();
        this.rutasCreadas = false;
    }

    @Override
    public void onBackPressed() {
        //Back button omitido
    }

    public void AutoCardView(View view) {
        RegistroAutoDialog registroAutoDialog = new RegistroAutoDialog();
        registroAutoDialog.show(getSupportFragmentManager(), "");
    }

    public void LugaresCardView(View v){
        if (this.usuario.getLugares().size() == 0) {
            nuevoAlert("No ha agregado direcciones.", "Entendido");
        } else {
            LugaresFrag lugaresFrag = new LugaresFrag();
            lugaresFrag.show(getSupportFragmentManager(), TAG);
        }
    }

    public void toMap(View v) {
        Intent intent = new Intent(this, MapsActiv.class);
        startActivity(intent);
    }

    public void HorarioCardView(View view) {
        RegistroHorarioDialog rhd = new RegistroHorarioDialog();
        rhd.show(getSupportFragmentManager(), "");
    }

    public void AvisoPrivacidadCardView(View v) {
        AvisoPrivacidadDialog avPriv = new AvisoPrivacidadDialog();
        avPriv.show(getSupportFragmentManager(), "");
    }
    public void registroInfo(View v){

        String msg = "<b>Disponibilidad de Auto</b> "
                +"<br>"
                +"Registre si <b>cuenta con un auto</b>, el <b>modelo</b>, <b>año</b>, y los <b>días</b> que lo ocupará para sus viajes.<br><br>"
                +"<b>Lugares de Llegada/Salida</b> "
                +"<br>"
                +"Registre sus <b>lugares</b>, tanto de los que sale hacia su " +
                "institución (eg. Casa), como los lugares a los que llega saliendo de su institución.<br><br>"
                +"<b>Horario</b> "
                +"<br>"
                +"Seleccione los días que asistirá a su institución <i>(los días en que registró su uso de auto ya se encuentran seleccionados y " +
                "marcados con el <b> ícono del auto </b> )</i>. <br><br> En la fila superior correspondiente al día, seleccione su <b>horario de llegada</b> a su " +
                "institución y el lugar de donde sale.<br><br> En la fila inferior seleccione su <b>horario de salida</b> de su institución y el lugar al" +
                "que llegará. "
                +"<br>";

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("Ayuda con el registro")
                .setMessage(Html.fromHtml(msg))
                .setPositiveButton("Aceptar",null)
                .show();
    }

    public boolean checarDatosAuto() {
        boolean resultado;

        if (this.usuario.getAutomovil().getModelo() != null) {
            if (this.usuario.getAutomovil().getModelo().isEmpty()) {
                llamarError(cvAuto, tvErrorDispAuto, "Ingrese el modelo del auto.");
                resultado = false;

            } else if (!(this.usuario.getAutomovil().getDisponibilidadPorDia()[DiaDeLaSemana.LUNES.ordinal()]
                    || this.usuario.getAutomovil().getDisponibilidadPorDia()[DiaDeLaSemana.MARTES.ordinal()]
                    || this.usuario.getAutomovil().getDisponibilidadPorDia()[DiaDeLaSemana.MIERCOLES.ordinal()]
                    || this.usuario.getAutomovil().getDisponibilidadPorDia()[DiaDeLaSemana.JUEVES.ordinal()]
                    || this.usuario.getAutomovil().getDisponibilidadPorDia()[DiaDeLaSemana.VIERNES.ordinal()]
                    || this.usuario.getAutomovil().getDisponibilidadPorDia()[DiaDeLaSemana.SABADO.ordinal()])) {
                llamarError(cvAuto, tvErrorDispAuto, "Seleccione los días que ocupa su carro.");
                resultado = false;

            } else {
                resetearError(tvErrorDispAuto);
                resultado = true;
            }

        } else {
            resetearError(tvErrorDispAuto);
            resultado = true;
        }

        return resultado;
    }

    public boolean checarDatosHorario() {
        boolean horarioRegistrado = false;
        for (int dia = 0; dia < DiaDeLaSemana.TOTAL_DIAS.ordinal(); dia++) {
            if (this.usuario.getHorariosLlegada()[dia] != null) {
                horarioRegistrado = true;
                if (this.usuario.getHorariosLlegada()[dia].getLugar() == null) {
                    llamarError(cvHorario, tvErrorHorario,
                            "No has seleccionado el lugar de llegada para tu traslado del día " +
                                    DiaDeLaSemana.values()[dia].name());
                    return false;
                }
                if (this.usuario.getHorariosSalida()[dia].getLugar() == null) {
                    llamarError(cvHorario, tvErrorHorario,
                            "No has seleccionado el lugar de salida para tu traslado del día " +
                                    DiaDeLaSemana.values()[dia].name());
                    return false;
                }
                if (!cbAvisoPrivacidad.isChecked()){
                    Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);
                    cbAvisoPrivacidad.setAnimation(shake);
                    return false;
                }
            }
        }

        if (!horarioRegistrado) {
            llamarError(cvHorario, tvErrorHorario,"No has registrado ningún horario");
            return false;
        }

        return true;
    }

    public boolean checarDatosLugares() {
        if (this.usuario.getLugares().isEmpty()) {
            llamarError(cvLugares, tvErrorLugares,"No has registrado ningún lugar");
            return false;
        }

        return true;
    }

    public void toMisRutas(View v) {

        if (checarDatosAuto() & checarDatosLugares() & checarDatosHorario()) {
            this.progressDialog.show();
            if (!rutasCreadas) {
                this.usuario.crearMisRutas(this);
            } else {
                this.dbUserDataRegistration.registrarDatosUsuario();
            }
        }
    }

    private void llamarError(CardView cv, TextView tv, String error) {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        cv.setAnimation(shake);
        tv.setText(error);
        tv.setTextColor(Color.RED);
        tv.setTextSize(10f);
    }

    private void resetearError(TextView tv) {
        tv.setText("");
    }

    private void nuevoAlert(String message, String messageButton) {
        new AlertDialog.Builder(this).setMessage(message).
                setNeutralButton(messageButton, null).show();
    }

    @Override
    public void onRutasCreadas(boolean creacionExitosa) {
        if (creacionExitosa) {
            this.rutasCreadas = true;
            Log.d(TAG, "Creacion de rutas exitosa");
            Log.d(TAG,  DataHolder.getInstance().getUsuario().toString());

            this.dbUserDataRegistration.registrarDatosUsuario();
        } else {
            this.progressDialog.dismiss();
            nuevoAlert("No se han podido crear las rutas para los días que registraste tu auto. " +
                    "Posiblemente hubo una falla con al consultar los lugares que proporcionaste. " +
                    "Inténtalo de nuevo, o intenta registrándote sin un auto. ", "Entendido");
            Log.d(TAG, "Creacion de rutas fallida");
        }
    }

    @Override
    public void onUserDataRegistrationCompleted(boolean registroExitoso) {
        if (registroExitoso) {
            ManejadorRutas manejadorRutas = new ManejadorRutas();
            manejadorRutas.importarRutas(this);
            Log.d(TAG, "Registro de datos exitoso");
        } else {
            nuevoAlert("Ha habido un problema registrando tu información a la base de datos. " +
                    "Checa tu conexión a internet o inténtalo de nuevo más tarde. ", "Entendido");
            Log.d(TAG, "Registro de datos fallido");
            this.progressDialog.dismiss();
        }
    }

    @Override
    public void onImportacionRutasConcluida(boolean importacionExitosa) {
        if (importacionExitosa) {
            DataHolder.getInstance().getUsuario().construirRutasCandidatas(this);
            Log.d(TAG, "Importación de rutas exitosa");
        } else {
            nuevoAlert("Ha habido un problema trayendo la información de la base de datos.", "Entendido");
            Log.d(TAG, "Importación de rutas fallida");
            this.progressDialog.dismiss();
        }
    }

    @Override
    public void onRutasCandidatasCosntruidas(boolean construccionExitosa) {
        this.progressDialog.dismiss();
        if (construccionExitosa) {
            Intent intent = new Intent(this, MisRutasActiv.class);
            startActivity(intent);
            Log.d(TAG, "Construcción exitosa");
        } else {
            nuevoAlert("Ha habido un problema trayendo la información de la base de datos", "Entendido");
            Log.d(TAG, "Construcción de rutas fallida");
        }
    }
}
