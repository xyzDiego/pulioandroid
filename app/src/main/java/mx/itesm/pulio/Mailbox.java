package mx.itesm.pulio;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedList;
import java.util.Queue;

import javax.annotation.Nullable;

public class Mailbox {

    private static final String TAG = "Mailbox";

    private final Queue<Notificacion> queueNotificaciones;
    private CollectionReference mailboxCollectionReference;

    private MailboxUpdateListener mailboxUpdateli;

    public Mailbox(String usuarioId) {
        this.queueNotificaciones = new LinkedList<>();
        this.mailboxCollectionReference = FirebaseFirestore.getInstance().collection("usuario").
                document(usuarioId).collection("mailbox");
    }

    public void setMailboxUpdateli(MailboxUpdateListener mailboxUpdateli) {
        this.mailboxUpdateli = mailboxUpdateli;
    }

    public void setMailboxListener() {
        this.mailboxCollectionReference.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                if (mailboxUpdateli == null) {
                    Log.w(TAG, "No se ha agregado el mailbox listener.", e);
                    return;
                }

                for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()) {
                    QueryDocumentSnapshot document = documentChange.getDocument();
                    switch (documentChange.getType()) {
                        case ADDED:
                            String idRemitente = document.get("idRemitente", String.class);
                            String idDestinatario = document.get("idDestinatario", String.class);
                            String idRuta = document.get("idRuta", String.class);
                            boolean isRequest = document.get("isRequest", Boolean.class);

                            if (isRequest) {
                                int numParada = document.get("numParada", Integer.class);
                                String lugarDireccion = document.get("lugarDireccion", String.class);
                                String lugarId = document.get("lugarId", String.class);
                                double lugarLatitud = document.get("lugarLatitud", Double.class);
                                double lugarLongitud = document.get("lugarLongitud", Double.class);
                                String pasajeroId = document.get("idPasajero", String.class);
                                String pasajeroNombre = document.get("pasajeroNombre", String.class);
                                long pasajeroTelefono = document.get("pasajeroTelefono", Long.class);

                                Ruta rutaNueva = new Ruta(DataHolder.getInstance().getUsuario().getOrganizacion().buscarRuta(idRuta));
                                Pasajero pasajero = new Pasajero(pasajeroId, pasajeroNombre, pasajeroTelefono);
                                Lugar lugar = new Lugar(lugarId, lugarDireccion, lugarLatitud, lugarLongitud);
                                Parada parada = new Parada(lugar, pasajero);
                                rutaNueva.getParadas().add(numParada, parada);

                                if (rutaNueva.getIsLlegada()) {
                                    rutaNueva.recalcularDistanciasYTiemposRutaLlegada(true);
                                } else {
                                    rutaNueva.recalcularDistanciasYTiemposRutaSalida(true);
                                }
                                NotificacionRequest notificacionRequest = new NotificacionRequest(idRemitente, idDestinatario, rutaNueva, numParada);
                                queueNotificaciones.add(notificacionRequest);
                            } else {
                                String choferNombre = document.get("choferNombre", String.class);
                                DiaDeLaSemana dia = document.get("dia", DiaDeLaSemana.class);
                                int hora = document.get("hora", Integer.class);
                                int minuto = document.get("minuto", Integer.class);
                                boolean aceptado = document.get("aceptado", Boolean.class);

                                NotificationAcknowledge notificationAcknowledge =
                                        new NotificationAcknowledge(idRemitente, idDestinatario, aceptado,
                                                idRuta, choferNombre, dia.name(), hora, minuto);
                                queueNotificaciones.add(notificationAcknowledge);
                            }
                            mailboxUpdateli.onMailboxUpdate();
                            Log.d(TAG, "Mailbox fue actualizado ADDED");
                            break;
                        case REMOVED:
                            Log.d(TAG, "Mailbox fue actualizado REMOVED");
                            break;
                        case MODIFIED:
                            Log.d(TAG, "Mailbox fue actualizado MODIFIED");
                            break;
                    }
                }
            }
        });
    }

    public Queue<Notificacion> getQueueNotificaciones() {
        return queueNotificaciones;
    }

    @Override
    public String toString() {
        return "Mailbox{" + "\n" +
                "queueNotificaciones=" + queueNotificaciones.toString() + "\n" +
                '}';
    }

    public interface MailboxUpdateListener {
        void onMailboxUpdate();
    }
}
