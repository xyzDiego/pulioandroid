package mx.itesm.pulio;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.chad.pulioapp.R;

import java.util.ArrayList;

public class SaveLocationFrag extends AppCompatDialogFragment {

    protected static final String TAG = "SaveLocationFrag";

    private EditText etName;
    private EditText etAddress;

    private ArrayList<Lugar> lugares;
    private int lastItem;

    @SuppressLint("NewApi")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_save_location, null);

        lugares = DataHolder.getInstance().getUsuario().getLugares();
        lastItem = lugares.size() - 1;

        etName = dialogView.findViewById(R.id.etNombre);

        etAddress = dialogView.findViewById(R.id.etDireccion);
        etAddress.setText(lugares.get(lastItem).getDireccion());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Guardar dirección");
        builder.setView(dialogView);

        builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (etName.getText().toString().equals("")) {
                    alert("Ingresa un nombre válido", "Entendido");
                } else {
                    lugares.get(lastItem).setId(etName.getText().toString());
                    toRegistroContActiv();
                }

            }
        });

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                lugares.remove(lastItem);
                dialogInterface.cancel();
            }
        });

        return builder.create();
    }

    private void alert(String message, String messageButton) {
        new AlertDialog.Builder(this.getContext()).setMessage(message).setNeutralButton(messageButton, null).show();
    }


    private void toRegistroContActiv() {
        Intent intent = new Intent(this.getContext(), RegistroContActiv.class);
        startActivity(intent);
    }

}
