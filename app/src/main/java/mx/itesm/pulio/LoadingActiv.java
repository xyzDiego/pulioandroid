package mx.itesm.pulio;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.pulioapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoadingActiv extends AppCompatActivity implements DatabaseLogin.LoginListener {

    private static final String TAG = "LoadingActiv";
    ImageView ivLoading;
    TextView tvLoading;
    FirebaseUser usuario = FirebaseAuth.getInstance().getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        ivLoading = findViewById(R.id.ivLoading);
        tvLoading = findViewById(R.id.tvLoading);
        ivLoading.startAnimation(
                AnimationUtils.loadAnimation(this, R.anim.anim_rotate_image));


        if(usuario != null){
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                DatabaseLogin dbLogin = new DatabaseLogin(this, usuario.getEmail());
                dbLogin.iniciarDescargaDatosUsuario();
            }
            else {
                tvLoading.setText("No tienes conexión a internet...");
                new AlertDialog.Builder(this).setMessage("Necesitas estar conectado a internet").
                        setNeutralButton("Entendido", null).show();
            }

        }
        else{
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                Intent intent = new Intent(this, InicioActiv.class);
                startActivity(intent);
            }
            else {
                tvLoading.setText("No tienes conexión a internet...");
                new AlertDialog.Builder(this).setMessage("Necesitas estar conectado a internet").
                        setNeutralButton("Entendido", null).show();
            }
        }
    }


    @Override
    public void onLoginCompleted(boolean autenticacionExitosa) {
        if (autenticacionExitosa){
            Intent intent = new Intent(this, MisRutasActiv.class);
            startActivity(intent);
            DataHolder.getInstance().getUsuario().prettyLogString(TAG);
        }
    }

    @Override
    public void onResetPasswordCompleted(boolean restauracionExitosa) {

    }
}
