package mx.itesm.pulio;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.LinkedList;

public class ManejadorRutas {

    private static final String TAG = "ManejadorRutas";

    private String idOrganizacion;
    private FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();

    //Este método se manda al final del proceso de LogIn o al final del proceso de registro
    //Su función principal es instanciar el objeto rutas de la organización descargando todas las rutas de la base de datos
    public void importarRutas(ManejadorRutasListener manejadorRutasli) {
        this.idOrganizacion = DataHolder.getInstance().getUsuario().getOrganizacion().getId();
        CollectionReference collection = firebaseFirestore.collection("organizacion")
                .document(idOrganizacion).collection("rutas");
        collection.get().addOnCompleteListener(new RutasRetrieval(manejadorRutasli));
    }

    private class RutasRetrieval implements OnCompleteListener<QuerySnapshot> {
        final LinkedList<Ruta> rutas;
        final ManejadorRutasListener manejadorRutasli;

	    public RutasRetrieval(ManejadorRutasListener manejadorRutasli) {
            this.rutas = DataHolder.getInstance().getUsuario().getOrganizacion().getRutas();
            this.manejadorRutasli = manejadorRutasli;
        }

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            int id;
            String choferId;
	        String choferNombre;
	        long choferTelefono;
	        int choferCapacidadAuto;
	        int inscritos;
	        DiaDeLaSemana dia;
	        boolean islLlegada;
            if (task.isSuccessful()) {
                SemaphoreConstruccionRutas semaphoreConstruccionRutas = new SemaphoreConstruccionRutas(task.getResult().size());

                for (int i = 0; i < task.getResult().size(); i++) {
                    DocumentSnapshot document = task.getResult().getDocuments().get(i);

                    id = Integer.parseInt(document.getId().substring(1));
                    choferId = document.get("choferId", String.class);
                    choferNombre = document.get("choferNombre", String.class);
                    choferTelefono = document.get("choferTelefono", Long.class);
                    choferCapacidadAuto = document.get("choferCapacidadAuto", Integer.class);
                    dia = document.get("dia", DiaDeLaSemana.class);
                    islLlegada = document.get("isLlegada", Boolean.class);
                    inscritos = document.get("inscritos", Integer.class);

                    Chofer choferNuevaRuta = new Chofer(choferId, choferNombre, choferTelefono, choferCapacidadAuto);
                    Ruta nuevaRuta = new Ruta(dia, islLlegada, choferNuevaRuta, inscritos, id);
                    rutas.add(nuevaRuta);

                    CollectionReference rutaParada = firebaseFirestore.collection("organizacion").document(idOrganizacion)
                            .collection("rutas").document(document.getId()).collection("paradas");
                    rutaParada.get().addOnCompleteListener(new ParadasRetrieval(manejadorRutasli, nuevaRuta, semaphoreConstruccionRutas));
                }
            } else {
                Log.e(TAG, " fallo al importar las rutas del sistema");
                manejadorRutasli.onImportacionRutasConcluida(false);
            }
        }
    }

    private class ParadasRetrieval implements OnCompleteListener<QuerySnapshot>{
        final ManejadorRutasListener manejadorRutasli;
        final Ruta ruta;
        final SemaphoreConstruccionRutas semaphoreConstruccionRutas;

	    public ParadasRetrieval(ManejadorRutasListener manejadorRutasli, Ruta ruta, SemaphoreConstruccionRutas semaphoreConstruccionRutas) {
            this.manejadorRutasli = manejadorRutasli;
            this.ruta = ruta;
            this.semaphoreConstruccionRutas = semaphoreConstruccionRutas;
        }

        @Override
        public void onComplete(@NonNull Task<QuerySnapshot> task) {
            if (task.isSuccessful() && !this.semaphoreConstruccionRutas.getFalloEnConstruccion()) {
                String lugarId;
                String lugarDireccion;
                String pasajeroId;
                String pasajeroNombre;
                double lugarLatitud;
                double lugarLongitud;
                double distanciaKm;
                long pasajeroTelefono;
                int hora;
                int minuto;
                Pasajero pasajeroParadaNueva;
                //T: El id del documento es el índice que tiene que ocupar la parada en el arreglo de rutas.getParadas()
                //	Así que primero se tiene que ordenar todos los documentos de task.getResult().getDocuments() por el id ascendentemente
                Parada[] paradas = new Parada[task.getResult().size()];
                for (DocumentSnapshot document : task.getResult()) {
                    lugarId = document.get("lugarId", String.class);
                    lugarDireccion = document.get("lugarDireccion", String.class);
                    pasajeroNombre = document.get("pasajeroNombre", String.class);
                    lugarLatitud = document.get("lugarLatitud", Double.class);
                    lugarLongitud = document.get("lugarLongitud", Double.class);
                    distanciaKm = document.get("distanciaKm", Double.class);
                    if(pasajeroNombre.equals("null")){
                        pasajeroParadaNueva = null;

                    }
                    else{
                        pasajeroId = document.get("idPasajero", String.class);
                        pasajeroTelefono = Long.parseLong(document.get("pasajeroTelefono").toString());
                        pasajeroParadaNueva = new Pasajero(pasajeroId, pasajeroNombre, pasajeroTelefono);
                    }
                    hora = document.get("hora", Integer.class);
                    minuto = document.get("minuto", Integer.class);

                    Lugar lugarParadaNueva = new Lugar(lugarId, lugarDireccion, lugarLatitud, lugarLongitud);
                    Parada paradaNueva = new Parada(lugarParadaNueva, pasajeroParadaNueva, hora, minuto, distanciaKm);
                    paradas[Integer.parseInt(document.getId())] = paradaNueva;
                }

                for(Parada parada : paradas) {
                    this.ruta.getParadas().add(parada);
                }

                if (this.semaphoreConstruccionRutas.incrementarRutasConstruidasYChecarSiElProcesoConluyo()) {
                    manejadorRutasli.onImportacionRutasConcluida(true);
                }

            } else if (task.isSuccessful() && this.semaphoreConstruccionRutas.getFalloEnConstruccion()) {
                //Do Nothing
            } else if (!task.isSuccessful() && !this.semaphoreConstruccionRutas.getFalloEnConstruccion()) {
                Log.d("ENTRO", "ENTRO");
                semaphoreConstruccionRutas.activarFallo();
                manejadorRutasli.onImportacionRutasConcluida(false);
            } else {
                //Do nothing
            }
        }
    }

    private class SemaphoreConstruccionRutas {
        //Clase usada para sincronizar y gestionar las rutas construidas asíncronamente
        //Cuando la variable rutasConstruidas  iguala rutasPorConstruir el proceso a concluido y el resultado depende
        //de la variable falloEnConstruccion

        private final int rutasPorConstruir;
        private int rutasConstruidas;
        private boolean falloEnConstruccion;

	    public SemaphoreConstruccionRutas(int rutasPorConstruir) {
            this.rutasPorConstruir = rutasPorConstruir;
            this.rutasConstruidas = 0;
            this.falloEnConstruccion = false;
        }

        public synchronized boolean getFalloEnConstruccion() {
            return this.falloEnConstruccion;
        }

        public synchronized boolean incrementarRutasConstruidasYChecarSiElProcesoConluyo() {
            this.rutasConstruidas++;
            return this.rutasConstruidas == this.rutasPorConstruir;
        }

        public synchronized void activarFallo() {
            this.falloEnConstruccion = true;
        }
    }

    public interface ManejadorRutasListener {
        void onImportacionRutasConcluida(boolean importacionExitosa);
    }

}
