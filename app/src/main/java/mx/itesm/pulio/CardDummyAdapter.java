package mx.itesm.pulio;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.pulioapp.R;

import java.util.ArrayList;
import java.util.List;

public class CardDummyAdapter extends RecyclerView.Adapter<CardDummyAdapter.CardViewHolder> {

    private final static String TAG = "CardDummyAdapter";

    private Context mCtx;
    private List<Ruta> rutaList;

    public CardDummyAdapter(Context mCtx, List<Ruta> rutaList) {
        this.mCtx = mCtx;
        this.rutaList = rutaList;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view =  inflater.inflate(R.layout.ruta_layout,null);

        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {

        holder.setIsRecyclable(false);

        Ruta ruta = rutaList.get(position);
        llenarInfoRuta(holder,ruta);
    }

    @Override
    public int getItemCount() {
        return rutaList.size();
    }

    class CardViewHolder extends RecyclerView.ViewHolder{

        LinearLayout linlay, linlayPas1,linlayPas2,linlayPas3,linlayPas4,linlayPas5;

        ArrayList<LinearLayout> layoutPasajeros = new ArrayList<>();

        ArrayList<TextView> nombrePasajeros = new ArrayList<>();
        ArrayList<TextView> direccionPasajeros = new ArrayList<>();
        ArrayList<TextView> distanciaPasajeros = new ArrayList<>();
        ArrayList<TextView> horaPasajeros = new ArrayList<>();

        ArrayList<ImageView> ivPasajeros = new ArrayList<>();
        ArrayList<ImageButton> ibDirPasajeros = new ArrayList<>();

        ImageView ivPasajero1,ivPasajero2,ivPasajero3,ivPasajero4,ivPasajero5;

        Button btnEstadoRuta;

        TextView tvDiaRuta, tvHoraRuta,
                tvNombrePasajero1, tvDirPasajero1, tvDistanciaPasajero1,tvHoraPasajero1,
                tvNombrePasajero2, tvDirPasajero2, tvDistanciaPasajero2,tvHoraPasajero2,
                tvNombrePasajero3, tvDirPasajero3, tvDistanciaPasajero3,tvHoraPasajero3,
                tvNombrePasajero4, tvDirPasajero4, tvDistanciaPasajero4,tvHoraPasajero4,
                tvNombrePasajero5, tvDirPasajero5, tvDistanciaPasajero5,tvHoraPasajero5,

        tvNombreDestino,tvDirDestino,tvDistanciaDestino,tvHoraDestino;

        CardView cvMiRuta;

        ImageButton ibDirPasajero1, ibDirPasajero2, ibDirPasajero3, ibDirPasajero4, ibDirPasajero5, ibDirDestino;


        @SuppressLint("CutPasteId")
        public CardViewHolder(View itemView) {

            super(itemView);


            linlay = itemView.findViewById(R.id.linlay);
            linlayPas1 = itemView.findViewById(R.id.linlayPas1);
            layoutPasajeros.add(linlayPas1);

            linlayPas2 = itemView.findViewById(R.id.linlayPas2);
            layoutPasajeros.add(linlayPas2);

            linlayPas3 = itemView.findViewById(R.id.linlayPas3);
            layoutPasajeros.add(linlayPas3);

            linlayPas4 = itemView.findViewById(R.id.linlayPas4);
            layoutPasajeros.add(linlayPas4);

            linlayPas5 = itemView.findViewById(R.id.linlayPas5);
            layoutPasajeros.add(linlayPas5);



            //Llenar Arreglo de Nombres
            tvNombrePasajero1 = itemView.findViewById(R.id.tvNombrePasajero1);
            nombrePasajeros.add(tvNombrePasajero1);

            tvNombrePasajero2 = itemView.findViewById(R.id.tvNombrePasajero2);
            nombrePasajeros.add(tvNombrePasajero2);

            tvNombrePasajero3 = itemView.findViewById(R.id.tvNombrePasajero3);
            nombrePasajeros.add(tvNombrePasajero3);

            tvNombrePasajero4 = itemView.findViewById(R.id.tvNombrePasajero4);
            nombrePasajeros.add(tvNombrePasajero4);

            tvNombrePasajero5 = itemView.findViewById(R.id.tvNombrePasajero5);
            nombrePasajeros.add(tvNombrePasajero5);



            btnEstadoRuta = itemView.findViewById(R.id.btnEstadoRuta);


            //Imagen del Pasajero
            ivPasajero1 = itemView.findViewById(R.id.ivPasajero1);
            ivPasajeros.add(ivPasajero1);

            ivPasajero2 = itemView.findViewById(R.id.ivPasajero2);
            ivPasajeros.add(ivPasajero2);

            ivPasajero3 = itemView.findViewById(R.id.ivPasajero3);
            ivPasajeros.add(ivPasajero3);

            ivPasajero4 = itemView.findViewById(R.id.ivPasajero4);
            ivPasajeros.add(ivPasajero4);

            ivPasajero5 = itemView.findViewById(R.id.ivPasajero5);
            ivPasajeros.add(ivPasajero5);


            tvDiaRuta =  itemView.findViewById(R.id.tvDiaRuta);
            tvHoraRuta =  itemView.findViewById(R.id.tvHoraRuta);


            //Llenar Arreglo de Direcciones
            tvDirPasajero1 =  itemView.findViewById(R.id.tvDirPasajero1);
            direccionPasajeros.add(tvDirPasajero1);

            tvDirPasajero2 =  itemView.findViewById(R.id.tvDirPasajero2);
            direccionPasajeros.add(tvDirPasajero2);

            tvDirPasajero3 =  itemView.findViewById(R.id.tvDirPasajero3);
            direccionPasajeros.add(tvDirPasajero3);

            tvDirPasajero4 =  itemView.findViewById(R.id.tvDirPasajero4);
            direccionPasajeros.add(tvDirPasajero4);

            tvDirPasajero5 =  itemView.findViewById(R.id.tvDirPasajero5);
            direccionPasajeros.add(tvDirPasajero5);


            //Llenar arreglo de distancias
            tvDistanciaPasajero1 =  itemView.findViewById(R.id.tvDistanciaPasajero1);
            distanciaPasajeros.add(tvDistanciaPasajero1);

            tvDistanciaPasajero2 =  itemView.findViewById(R.id.tvDistanciaPasajero2);
            distanciaPasajeros.add(tvDistanciaPasajero2);

            tvDistanciaPasajero3 =  itemView.findViewById(R.id.tvDistanciaPasajero3);
            distanciaPasajeros.add(tvDistanciaPasajero3);

            tvDistanciaPasajero4 =  itemView.findViewById(R.id.tvDistanciaPasajero4);
            distanciaPasajeros.add(tvDistanciaPasajero4);

            tvDistanciaPasajero5 =  itemView.findViewById(R.id.tvDistanciaPasajero5);
            distanciaPasajeros.add(tvDistanciaPasajero5);

            //Hora del Pasajero
            tvHoraPasajero1 =  itemView.findViewById(R.id.tvHoraPasajero1);
            horaPasajeros.add(tvHoraPasajero1);

            tvHoraPasajero2 =  itemView.findViewById(R.id.tvHoraPasajero2);
            horaPasajeros.add(tvHoraPasajero2);

            tvHoraPasajero3 =  itemView.findViewById(R.id.tvHoraPasajero3);
            horaPasajeros.add(tvHoraPasajero3);

            tvHoraPasajero4 =  itemView.findViewById(R.id.tvHoraPasajero4);
            horaPasajeros.add(tvHoraPasajero4);

            tvHoraPasajero5 =  itemView.findViewById(R.id.tvHoraPasajero5);
            horaPasajeros.add(tvHoraPasajero5);


            //Boton de la direccion del pasajero
            ibDirPasajero1 = itemView.findViewById(R.id.ibDirPasajero1);
            ibDirPasajeros.add(ibDirPasajero1);

            ibDirPasajero2 = itemView.findViewById(R.id.ibDirPasajero2);
            ibDirPasajeros.add(ibDirPasajero2);

            ibDirPasajero3 = itemView.findViewById(R.id.ibDirPasajero3);
            ibDirPasajeros.add(ibDirPasajero3);

            ibDirPasajero4 = itemView.findViewById(R.id.ibDirPasajero4);
            ibDirPasajeros.add(ibDirPasajero4);

            ibDirPasajero5 = itemView.findViewById(R.id.ibDirPasajero5);
            ibDirPasajeros.add(ibDirPasajero5);

            tvNombreDestino = itemView.findViewById(R.id.tvNombreDestino);
            tvDirDestino = itemView.findViewById(R.id.tvDirDestino);
            tvDistanciaDestino = itemView.findViewById(R.id.tvDistanciaDestino);
            tvHoraDestino = itemView.findViewById(R.id.tvHoraDestino);
            ibDirDestino = itemView.findViewById(R.id.ibDirDestino);

            cvMiRuta = itemView.findViewById(R.id.cvMiRuta);
        }
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    private void llenarInfoRuta(CardViewHolder holder, Ruta ruta) {

        //Asignar Dia
        holder.tvDiaRuta.setText(String.valueOf(ruta.getDia()));


        //Ajustar Card para No. de Pasajeros
        int cerrarLayouts = 5-(ruta.getInscritos()-1);

        Log.i("----SS",ruta.getParadas().toString());
        Log.i("----DD", String.valueOf(ruta.getInscritos()));
        Log.i("----DD", String.valueOf(holder.layoutPasajeros.size()));

        for (int i = 4; i>=ruta.getParadas().size()-2; i--) {

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    0,
                    0.0f
            );
            holder.layoutPasajeros.get(i).setLayoutParams(param);

            //Llenar info de Usuarios, PENDIENTE

        }

        for (int i = 1; i < ruta.getParadas().size()-1; i++) {
            holder.nombrePasajeros.get(i - 1).setText(ruta.getParadas().get(i).getPasajero().getNombre());
            holder.direccionPasajeros.get(i - 1).setText(ruta.getParadas().get(i).getLugar().getDireccion());
            holder.distanciaPasajeros.get(i-1).setText(String.valueOf(ruta.getParadas().get(i).getDistanciaKm())+" KM");
            holder.horaPasajeros.get(i-1).setText(String.format("%02d:%02d",ruta.getParadas().get(i).getHora(),ruta.getParadas().get(i).getMinuto()));

            int finalI = i;
            holder.ivPasajeros.get(i-1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String pasajero = ruta.getParadas().get(finalI).getPasajero().getNombre();
                    String numero = String.valueOf(ruta.getParadas().get(finalI).getPasajero().getTelefono());


                    mostrarInfoPasajero(pasajero,numero,false);

                }
            });

            int indice = i;
            holder.ibDirPasajeros.get(i-1).setOnClickListener(v -> {
                String url = "https://www.google.com/maps/search/?api=1&query=" +
                        ruta.getParadas().get(indice).getLugar().getLatitud() + "," +
                        ruta.getParadas().get(indice).getLugar().getLongitud();

                Uri gmmIntentUri = Uri.parse(url);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mCtx.startActivity(mapIntent);
            });

        }

        //Ajustar altura de Card --CAMBIAR SI ACTUA RARO CON MAS PASAJEROS
        LinearLayout.LayoutParams cardViewParams =
                new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        500+(200*ruta.getInscritos()));

        holder.cvMiRuta.setLayoutParams(cardViewParams);

        ViewGroup.MarginLayoutParams cardViewMarginParams = (ViewGroup.MarginLayoutParams) holder.cvMiRuta.getLayoutParams();
        cardViewMarginParams.setMargins(36, 36, 36, 36);
        holder.cvMiRuta.requestLayout();

        //Asignar Ida/Vuelta
        if (ruta.getIsLlegada()) {
            holder.linlay.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.colorAccent));
        } else {
            holder.linlay.setBackgroundColor(ContextCompat.getColor(mCtx, R.color.colorOrange));
        }

        //Boton de Info. Conductor / Pasajero
        holder.btnEstadoRuta.setText(ruta.getChofer().getNombre());

        //Horario de Inicio de Ruta
        holder.tvHoraRuta.setText(ruta.getParadas().get(0).getHora() + ":" +
                String.format("%02d", ruta.getParadas().get(0).getMinuto()));

        ///DESTINO

        //Titulo Destino
        holder.tvNombreDestino.setText(ruta.getParadas().get(ruta.getParadas().size() - 1).getLugar().getId());
        //Direccion Destino
        holder.tvDirDestino.setText(ruta.getParadas().get(ruta.getParadas().size() - 1).getLugar().getDireccion());
        //Distancia al Destino
        holder.tvDistanciaDestino.setText(ruta.getParadas().get(ruta.getParadas().size() - 1).getDistanciaKm() + " KM");
        //Hora para llegar al destino
        holder.tvHoraDestino.setText(ruta.getParadas().get(ruta.getParadas().size() - 1).getHora() + ":" +
                String.format("%02d", ruta.getParadas().get(ruta.getParadas().size() - 1).getMinuto())

        );

        //Boton Lugar Destino
        holder.ibDirDestino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Parada> paradas = ruta.getParadas();
                int numeroElementos = paradas.size() - 1;

                Parada primeraParada = paradas.get(0);
                Parada ultimaParada = paradas.get(numeroElementos);

                String url = "https://www.google.com/maps/dir/?api=1" +
                        "&origin=" + primeraParada.getLugar().getLatitud() + "," + primeraParada.getLugar().getLongitud() +
                        "&destination=" + ultimaParada.getLugar().getLatitud() + "," + ultimaParada.getLugar().getLongitud() +
                        "&travelmode=" + "driving" +
                        "&waypoints=";

                for (int indice = 1; indice < numeroElementos; indice++) {
                    url += "|" + paradas.get(indice).getLugar().getLatitud() + "," + paradas.get(indice).getLugar().getLongitud();
                }

                Log.d(TAG, url);

                Uri gmmIntentUri = Uri.parse(url);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                mCtx.startActivity(mapIntent);
            }
        });
        //Boton Info Contacto
        holder.btnEstadoRuta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String conductor = ruta.getChofer().getNombre();
                String numero = String.valueOf(ruta.getChofer().getTelefono());
                mostrarInfoPasajero(conductor,numero,true);
            }
        });
    }

    private void mostrarInfoPasajero(String nombre, String numero,boolean esChofer){

        String titulo = esChofer ? "Conductor " : "Pasajero ";

        String msg = "<b>"+titulo+"</b>"
                +nombre
                +"<br>"
                +"Número de teléfono: "+numero;

        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        builder.setTitle("Información del "+titulo)
                .setMessage(Html.fromHtml(msg))
                .setNeutralButton("Aceptar",null)
                .setNegativeButton("Enviar WhatsApp", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String url = "https://api.whatsapp.com/send?phone=" + "+52 "+numero;
                        try {
                            PackageManager pm = mCtx.getPackageManager();
                            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(url));
                            mCtx.startActivity(i);
                        } catch (PackageManager.NameNotFoundException e) {
                            Toast.makeText(mCtx, "Whatsapp no está instalado", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                })
                .setPositiveButton("Llamar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:"+numero));
                        mCtx.startActivity(intent);
                    }
                })
                .show();
    }



}
