package mx.itesm.pulio;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.chad.pulioapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;

public class MisRutasActiv extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        Mailbox.MailboxUpdateListener,
        AcknowledgeDialog.ResponseListener,
        DatabaseActualizacionRuta.ActualizacionRutaListener,
        DatabaseNotificationSender.DatabaseNotificationSenderListener {

    private static final String TAG = "MisRutasActiv";

    RecyclerView recyclerView;
    CardDummyAdapter adapter;
    List<Ruta> rutasList;

    Button btnActualizar;
    TextView tvNoRutas,tvMisRutasFecha, tvSaludo;
    Button btnRutas;

    private Usuario usuario;
    private ArrayList<Boolean> diasRegistrados;
    private ArrayList<DiaDeLaSemana> dias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_rutas);

        rutasList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this ));

        btnActualizar = findViewById(R.id.btnActualizar);
        tvNoRutas = findViewById(R.id.tvNoRutas);
        tvMisRutasFecha = findViewById(R.id.tvMisRutasFecha);


        Date today = Calendar.getInstance().getTime();//getting date
        SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMMM dd", Locale.getDefault());//formating according to my need
        String date = formatter.format(today);
        tvMisRutasFecha.setText(date);

        tvSaludo = findViewById(R.id.tvSaludo);
        tvSaludo.append(", "+DataHolder.getInstance().getUsuario().getNombre()+"!");

        btnRutas = findViewById(R.id.btnRutas);

        llenarListaRutas();

        if (rutasList.size() == 0){
            recyclerView.setVisibility(View.INVISIBLE);

            tvNoRutas.setVisibility(View.VISIBLE);
            btnRutas.setVisibility(View.VISIBLE);

        }else{
            recyclerView.setVisibility(View.VISIBLE);

            tvNoRutas.setVisibility(View.INVISIBLE);
            btnRutas.setVisibility(View.INVISIBLE);
        }

        this.setTitle("Mis Rutas");

        adapter = new CardDummyAdapter(this,rutasList);
        recyclerView.setAdapter(adapter);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        btnActualizar.setVisibility(View.INVISIBLE);
        btnActualizar.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), LoadingActiv.class);
            DataHolder.getInstance().clearSession();
            startActivity(intent);
            //finish();
        });

        this.usuario = DataHolder.getInstance().getUsuario();
        this.usuario.getMailbox().setMailboxUpdateli(this);
        this.usuario.getMailbox().setMailboxListener();
        procesarNotificationQueue();
    }

    private void procesarNotificationQueue() {
        Queue<Notificacion> queue = this.usuario.getMailbox().getQueueNotificaciones();

        while (!queue.isEmpty()) {
            Notificacion notificacion = queue.remove();
            if (notificacion.isRequest()) {
                NotificacionRequest notificacionRequest = (NotificacionRequest) notificacion;

                AcknowledgeDialog acknowledgeDialog = new AcknowledgeDialog();
                acknowledgeDialog.setNotificacionRequest(notificacionRequest);
                acknowledgeDialog.setCancelable(false);
                acknowledgeDialog.setAcknowledgeDialogResponseListener(this);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(acknowledgeDialog, "acknowledgeDialog");
                ft.commitAllowingStateLoss();

            } else {
                NotificationAcknowledge notificationAcknowledge = (NotificationAcknowledge) notificacion;

                @SuppressLint("DefaultLocale")
                String mensaje = notificationAcknowledge.getChoferNombre()
                        + " ha " + (notificationAcknowledge.isAceptado() ? "aceptado" : "rechazado")
                        + " tu solicitud para la ruta del día "
                        + notificationAcknowledge.getDia().toLowerCase() + " a las "
                        + String.format("%02d:%02d", notificationAcknowledge.getHora(),
                        notificationAcknowledge.getMinuto());

                //Checar si el usuario ya tiene en sus arrelgos las rutas cuyas solicitudes
                //fueron aceptadas, de lo contrario, es necesario refrescar
                if (this.btnActualizar.getVisibility() == View.INVISIBLE
                        && notificationAcknowledge.isAceptado()) {
                    boolean isUpdated = false;
                    List<Ruta> rutas = new LinkedList();
                    rutas.addAll(usuario.getRutasLlegada());
                    rutas.addAll(usuario.getRutasSalida());
                    for (Ruta ruta : rutas) {
                        if (("r" + ruta.getId()).equals(notificationAcknowledge.getIdRuta())) {
                            isUpdated = true;
                            break;
                        }
                    }
                    if (!isUpdated)
                        this.btnActualizar.setVisibility(View.VISIBLE);
                }

                new AlertDialog.Builder(this).setCancelable(false)
                        .setMessage(mensaje).
                        setNeutralButton("Entendido.", (dialogInterface, i) ->
                                FirebaseFirestore.getInstance().collection("usuario")
                                .document(usuario.getId()).collection("mailbox")
                                .document(notificacion.getIdRemitente()
                                        + notificacion.getIdDestinatario()
                                        + notificationAcknowledge.getIdRuta().substring(1)
                                        + "a").delete()).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
        }
    }

    private void llenarListaRutas(){

        this.rutasList = new ArrayList<>();

        rutasList.addAll(DataHolder.getInstance().getUsuario().getRutasLlegada());
        rutasList.addAll(DataHolder.getInstance().getUsuario().getRutasSalida());

        Calendar calendar = Calendar.getInstance();
        int diaNum = calendar.get(Calendar.DAY_OF_WEEK);

        Collections.sort(rutasList);
        //Debug
        diasRegistrados = new ArrayList<>();
        dias = new ArrayList<>();
        Log.i(">>>",String.valueOf(DataHolder.getInstance().getUsuario().getRutasCandidatas()));
        sortRutas();
        checarAccesoARutas();
        //PENDIENTE

    }
    public void prueba (View v){
        Intent intent = new Intent(this, RutasActiv.class);
        intent.putExtra("diasRegistrados",diasRegistrados);
        intent.putExtra("dias",dias);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {


        } else if (id == R.id.nav_gallery) {

            Intent intent = new Intent(this,RutasActiv.class);
            intent.putExtra("diasRegistrados",diasRegistrados);
            intent.putExtra("dias",dias);
            startActivity(intent);

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.log_out){
            FirebaseAuth.getInstance().signOut();
            DataHolder.getInstance().clearSession();
            Intent intent = new Intent(this, LoadingActiv.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMailboxUpdate() {
        procesarNotificationQueue();
    }

    @Override
    public void onAcknowledgeResponse(NotificacionRequest notificacionRequest, boolean aceptado) {
        FirebaseFirestore.getInstance()
                .collection("usuario")
                .document(this.usuario.getId())
                .collection("mailbox")
                .document(notificacionRequest.getIdRemitente()
                        + notificacionRequest.getIdDestinatario()
                        + notificacionRequest.getRuta().getId() + "r").delete();

        if (aceptado) {
            notificacionRequest.getRuta().setInscritos(notificacionRequest.getRuta().getInscritos() + 1);
            Log.d(TAG, " necesita update");
            this.btnActualizar.setVisibility(View.VISIBLE);
            (new DatabaseActualizacionRuta()).actualizarRuta(this, notificacionRequest);
        } else {
            (new DatabaseNotificationSender()).sendNotificacionAcknowledege(this,
                    notificacionRequest, false);
        }

    }

    @Override
    public void onRutaActualizada(boolean actualizacionExitosa, NotificacionRequest notificacionRequest)  {
        if (actualizacionExitosa) {
            (new DatabaseNotificationSender()).sendNotificacionAcknowledege(this,
                    notificacionRequest, true);
        }
    }

    @Override
    public void onNotificationSend(boolean operacionExitosa, Ruta ruta) {
        if (operacionExitosa){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Se ha notificado al usuario de su respuesta.")
                    .setPositiveButton("Aceptar", null)
                    .show();

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("No se ha podido notificar al usuario de su respuesta.")
                    .setPositiveButton("Aceptar",null)
                    .show();
        }
    }
    private void checarAccesoARutas(){

        for (int i = 0; i<DataHolder.getInstance().getUsuario().getHorariosLlegada().length;i++){

            if (DataHolder.getInstance().getUsuario().getHorariosLlegada()[i]!=null) {
                diasRegistrados.add(false);
                diasRegistrados.add(false);
                if (!dias.contains(DiaDeLaSemana.values()[i]))
                    dias.add(DiaDeLaSemana.values()[i]);
            }


        }


    }
    private void sortRutas(){
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)-2;
        int r = 0;
        Log.i("Day",String.valueOf(day));

        for (int i = 0; i<rutasList.size(); i++){
            if (DiaDeLaSemana.valueOf(String.valueOf(rutasList.get(i).getDia())).ordinal() < day){

                r+=1;

            }else{

                break;

            }
        }
        Collections.rotate(rutasList,-r);


    }

}
