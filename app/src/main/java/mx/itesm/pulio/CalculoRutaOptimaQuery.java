package mx.itesm.pulio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class CalculoRutaOptimaQuery extends Thread {

    private static final String TAG = "CalculoRutaOptimaQuery";

    private Ruta ruta;
    private StatusRequest status;

    public CalculoRutaOptimaQuery(Ruta ruta) {
        this.ruta = ruta;
    }

    @Override
    public void run() {
        ArrayList<Parada> paradas = this.ruta.getParadas();

        Parada primeraParada = paradas.get(0);
        Parada ultimaParada = paradas.get(this.ruta.getParadas().size() - 1);

        String targetURL = "https://maps.googleapis.com/maps/api/directions/json?" +
                "origin=" + primeraParada.getLugar().getLatitud() + "," + primeraParada.getLugar().getLongitud() +
                "&destination=" + ultimaParada.getLugar().getLatitud() + "," + ultimaParada.getLugar().getLongitud() +
                "&waypoints=optimize:true";

        int penultimoDestino = this.ruta.getParadas().size() - 1;
        for (int indiceParada = 1; indiceParada < penultimoDestino; indiceParada++) {
            targetURL += "|" + paradas.get(indiceParada).getLugar().getLatitud() + "," +
                    paradas.get(indiceParada).getLugar().getLongitud();
        }

        targetURL += "&key=AIzaSyBl0ZhkjDBRYeNRwVOfDmrc-VcPhDA-FNg";

        try {
            URL url = new URL(targetURL);

            URLConnection conn = url.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            StringBuilder response = new StringBuilder();

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json = new JSONObject(response.toString());
            String statusRequest = json.getString("status");
            obtenerInformationStatus(statusRequest);

            if (this.status == StatusRequest.OK) {
                JSONArray puntosIntermedios = json.getJSONArray("routes").getJSONObject(0).
                        getJSONArray("waypoint_order");

                int[] ordenPuntosIntermedios = obtenerOrdenPuntosIntermedios(puntosIntermedios);

                if (ordenPuntosIntermedios.length > 1) {
                    ArrayList<Parada> paradasTemporales = new ArrayList<>();
                    for (int indice = 1; indice < penultimoDestino; indice++)
                        paradasTemporales.add(paradas.get(indice));

                    for (int indice = 1; indice < penultimoDestino; indice++)
                        paradas.set(indice, paradasTemporales.get(ordenPuntosIntermedios[indice - 1]));
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            this.status = StatusRequest.MALFORMED_URL;
        } catch (IOException e) {
            e.printStackTrace();
            this.status = StatusRequest.IO_EXCEPTION;
        } catch (Exception e) {
            e.printStackTrace();
            this.status = StatusRequest.UNKNOWN_EXCEPTION;
        }

    }

    public void obtenerInformationStatus(String statusRequest) {
        switch (statusRequest) {
            case "OK":
                this.status = StatusRequest.OK;
                break;
            case "ZERO_RESULTS":
                this.status = StatusRequest.ZERO_RESULTS;
                break;
            case "OVER_QUERY_LIMIT":
                this.status = StatusRequest.OVER_QUERY_LIMIT;
                break;
            case "REQUEST_DENIED":
                this.status = StatusRequest.REQUEST_DENIED;
                break;
            case "INVALID_REQUEST":
                this.status = StatusRequest.INVALID_REQUEST;
                break;
            default:
                this.status = StatusRequest.UNKNOWN_ERROR;
                break;
        }
    }

    private int[] obtenerOrdenPuntosIntermedios(JSONArray puntosIntermedios) throws JSONException {
        int[] puntosIntermediosPrimitivo = new int[puntosIntermedios.length()];

        for (int indice = 0; indice < puntosIntermediosPrimitivo.length; indice++)
            puntosIntermediosPrimitivo[indice] = puntosIntermedios.getInt(indice);

        return puntosIntermediosPrimitivo;
    }

}
