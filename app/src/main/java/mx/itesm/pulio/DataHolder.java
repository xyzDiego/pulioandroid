package mx.itesm.pulio;

public class DataHolder {

    private static DataHolder instance;

    private Usuario usuario = null;

    public static DataHolder getInstance() {
        if (instance == null) {
            instance = new DataHolder();
        }

        return instance;
    }

    private DataHolder() {}

    public Usuario getUsuario() {
        if (usuario == null) {
            throw new NullPointerException("El usuario no se ha creado");
        }

        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        if (this.usuario != null) {
            throw new ExceptionInInitializerError("El usuario solo se puede inicializar una vez");
        }

        this.usuario = usuario;
    }

    public void clearSession() {
        this.usuario = null;
    }

}
