package mx.itesm.pulio;

/**
 * Dado un punto de origen y de destino y un tiempo de salida, calcula el tiempo de llegada,
 * y la distancia entre ambos puntos.
 */

public class TiempoSalidaQuery extends TiempoQuery {

    private int origenHora;
    private int origenMinuto;
    private int destinoHora;
    private int destinoMinuto;

    public TiempoSalidaQuery(double origenLatitud, double origenLongitud, double destinoLatitud, double destinoLongitud,
                      int origenHora, int origenMinuto, DiaDeLaSemana diaDeLaSemana) {
        super(origenLatitud, origenLongitud, destinoLatitud, destinoLongitud, diaDeLaSemana);
        this.origenHora = origenHora;
        this.origenMinuto = origenMinuto;
    }

    @Override
    public void obtenerInformacionDuracion(String strDuration) {
        int duration = Integer.parseInt(strDuration.split(" ")[0]);
        int tiempoMinutosDestino = (this.origenHora * 60) + this.origenMinuto;
        int tiempoMinutosOrigen = tiempoMinutosDestino + duration;

        this.destinoHora = tiempoMinutosOrigen / 60;
        this.destinoMinuto = tiempoMinutosOrigen % 60;
    }

    public int getDestinoHora() {
        return destinoHora;
    }

    public int getDestinoMinuto() {
        return destinoMinuto;
    }

    @Override
    public String toString() {
        return super.toString() + " " +
                "TiempoSalidaQuery{" +
                "origenHora=" + origenHora +
                ", origenMinuto=" + origenMinuto +
                ", destinoHora=" + destinoHora +
                ", destinoMinuto=" + destinoMinuto +
                '}';
    }
}