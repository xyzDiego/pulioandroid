package mx.itesm.pulio;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.chad.pulioapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LugaresFrag extends AppCompatDialogFragment {

    protected ListView lvLugares;
    protected ArrayList<Lugar> lugares;
    private String tituloFrag;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_lugares, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        lugares = DataHolder.getInstance().getUsuario().getLugares();

        tituloFrag = "Lugares de llegada/salida";

        lvLugares = dialogView.findViewById(R.id.lvLugares);

        HashMap<String, String> hashDirecciones = new HashMap<>();
        for (Lugar lugar : lugares) {
            hashDirecciones.put(lugar.getId(), lugar.getDireccion());
        }

        ArrayList<HashMap<String, String>> listaDirecciones = new ArrayList<>();
        SimpleAdapter adapter = new SimpleAdapter(this.getContext(), listaDirecciones, R.layout.list_items,
                new String[]{"Nombre", "Direccion"},
                new int[]{R.id.tvTitulo, R.id.tvDireccion});

        Iterator iterator = hashDirecciones.entrySet().iterator();
        while (iterator.hasNext()) {
            HashMap<String, String> hashTemporal = new HashMap<>();
            Map.Entry direccionTemp = (Map.Entry) iterator.next();
            hashTemporal.put("Nombre", direccionTemp.getKey().toString());
            hashTemporal.put("Direccion", direccionTemp.getValue().toString());
            listaDirecciones.add(hashTemporal);
        }

        builder.setTitle(tituloFrag);
        builder.setView(dialogView);
        builder.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        lvLugares.setAdapter(adapter);
        return builder.create();
    }


}
