package mx.itesm.pulio;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.pulioapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RutasActiv extends AppCompatActivity implements DatabaseNotificationSender.DatabaseNotificationSenderListener {

    RecyclerView rvRutas;
    RutaAdapter adapter;
    ArrayAdapter<String> ad;
    ArrayList<Ruta> rutaDummyList;
    ArrayList<Ruta> rutaSeleccionadaList;
    ArrayList<Ruta> filteredList;
    List<String> DiasDeTrabajo;

    ArrayList<Boolean> diasRegistrados;
    ArrayList<DiaDeLaSemana> dias;

    Button btnRutaIdaVuelta;

    Spinner spnDias;
    String diaFiltro;

    TextView tvHoraDeEntrada, tvHoraDeSalida;

    boolean ddtHasIndex;
    int valueOfRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rutas);

        btnRutaIdaVuelta = findViewById(R.id.btnRutaIdaVuelta);
        valueOfRegister = 0;


        int horarios = DataHolder.getInstance().getUsuario().getHorariosSalida().length;

        DiasDeTrabajo = new ArrayList<String>();

        diasRegistrados = (ArrayList<Boolean>) getIntent().getSerializableExtra("diasRegistrados");
        dias = (ArrayList<DiaDeLaSemana>) getIntent().getSerializableExtra("dias");


       for (int i = 0; i < dias.size(); i++) {
            if (!diasRegistrados.get(i * 2) || !diasRegistrados.get((i * 2) + 1))
                DiasDeTrabajo.add(String.valueOf(dias.get(i)));
       }


        if (DiasDeTrabajo.isEmpty()) {
            finish();
            Toast.makeText(this, "Ya no te quedan días por inscribir.", Toast.LENGTH_SHORT).show();
        }

        ad = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, DiasDeTrabajo);

        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnDias = (Spinner) findViewById(R.id.spnDias);
        spnDias.setAdapter(ad);

        tvHoraDeEntrada = findViewById(R.id.tvHoraDeEntrada);
        tvHoraDeSalida = findViewById(R.id.tvHoraDeSalida);

        spnDias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                // filter(DiasDeTrabajo.get(position),true);
                diaFiltro = DiasDeTrabajo.get(position).toString().toLowerCase();

                tvHoraDeEntrada.setText(String.format("%02d:%02d",
                        DataHolder.getInstance().getUsuario().getHorariosLlegada()[DiaDeLaSemana.valueOf(DiasDeTrabajo.get(position)).ordinal()].getHora(),
                        DataHolder.getInstance().getUsuario().getHorariosLlegada()[DiaDeLaSemana.valueOf(DiasDeTrabajo.get(position)).ordinal()].getMinuto()));

                tvHoraDeSalida.setText(String.format("%02d:%02d",
                        DataHolder.getInstance().getUsuario().getHorariosSalida()[DiaDeLaSemana.valueOf(DiasDeTrabajo.get(position)).ordinal()].getHora(),
                        DataHolder.getInstance().getUsuario().getHorariosSalida()[DiaDeLaSemana.valueOf(DiasDeTrabajo.get(position)).ordinal()].getMinuto()));


                for (int i = 0; i < dias.size(); i++) {

                    if (String.valueOf(dias.get(i)).equals(String.valueOf(spnDias.getSelectedItem()))) {
                        valueOfRegister = i;
                    }

                }

                if (!diasRegistrados.get(valueOfRegister * 2 + 1)) {

                    btnRutaIdaVuelta.setText("ida");
                    btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorAccent));

                    filter(DiasDeTrabajo.get(position), true);

                } else {

                    btnRutaIdaVuelta.setText("vuelta");
                    btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorOrange));

                    filter(DiasDeTrabajo.get(position), false);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });


        rutaDummyList = new ArrayList<>();
        rutaSeleccionadaList = new ArrayList<>();

        poblarLista();


        rvRutas = (RecyclerView) findViewById(R.id.rvRutas);
        rvRutas.setHasFixedSize(true);
        rvRutas.setLayoutManager(new LinearLayoutManager(this));


        adapter = new RutaAdapter(this, rutaDummyList, rutaSeleccionadaList);
        rvRutas.setAdapter(adapter);

        adapter.setOnItemClickListener(new RutaAdapter.OnItemClickListener() {

            @Override
            public void onItemClick(int position) {


                //Log.i("Dias",String.valueOf(dias));
                //Log.i("Dias",String.valueOf(diasRegistrados));

                //actualizarVisibilidadRutas();

            }

            @Override
            public void onSelectClick(int position) {

                Collections.sort(rutaSeleccionadaList);

                if (!rutaSeleccionadaList.contains(filteredList.get(position))) {
                    for (int i = 0; i < rutaSeleccionadaList.size(); i++) {

                        if (filteredList.get(position).getDia() == rutaSeleccionadaList.get(i).getDia()
                                && filteredList.get(position).getIsLlegada() == rutaSeleccionadaList.get(i).getIsLlegada()) {
                            rutaSeleccionadaList.remove(rutaSeleccionadaList.get(i));
                        }
                    }
                    rutaSeleccionadaList.add(filteredList.get(position));

                } else {
                    rutaSeleccionadaList.remove(filteredList.get(position));
                }


                adapter.filterList(filteredList);


            }
        });
    }

    public void confirmarRutas(View v) {
        String rutas = "";
        if (rutaSeleccionadaList.size() == 0) {

            rutas = "No ha seleccionado rutas.";
            new AlertDialog.Builder(this)
                    .setTitle("Rutas")
                    .setMessage(Html.fromHtml(rutas))
                    .setNegativeButton("Volver", null)
                    .show();
        } else {
            for (int i = 0; i < rutaSeleccionadaList.size(); i++) {


                String num = String.valueOf(rutaSeleccionadaList.get(i).getParadas().get(0).getMinuto());
                rutas = rutas.concat(
                        "<br /><b>" + rutaSeleccionadaList.get(i).getDia().toString() + "</b>"
                                + " "
                                + String.valueOf(rutaSeleccionadaList.get(i).getParadas().get(0).getHora())
                                + ":" + String.format("%02d", rutaSeleccionadaList.get(i).getParadas().get(0).getMinuto())
                                + "<br />"
                                + "Conductor: " + String.valueOf(rutaSeleccionadaList.get(i).getChofer().getNombre())
                                + "<br />"
                                + "Con rumbo a: "
                                + String.valueOf(rutaSeleccionadaList.get(i).getParadas().get(rutaSeleccionadaList.get(i).getParadas().size() - 1).getLugar().getDireccion())
                                + "<br /> "
                );
            }

            new AlertDialog.Builder(this)
                    .setTitle("Ha seleccionado las siguientes rutas:")
                    .setMessage(Html.fromHtml(rutas))
                    .setPositiveButton("Registrar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            enviarInfoRutas();
                            //actualizarVisibilidadRutas();
                        }
                    })
                    .setNegativeButton("Volver", null)
                    .show();
        }
    }

    private void actualizarVisibilidadRutas() {
        //Actualizar rutas
        for (int i = 0; i < rutaSeleccionadaList.size(); i++) {

            DiaDeLaSemana dia = rutaSeleccionadaList.get(i).getDia();

            for (int j = 0; j<dias.size();j++){
                if (dia.equals(dias.get(j))){
                    if (rutaSeleccionadaList.get(i).getIsLlegada()){
                        diasRegistrados.set(j*2,true);
                    }else{
                        diasRegistrados.set(j*2+1,true);
                    }
                   funct(j);
                }
            }
        }
    }

    private void funct(int j){

        if (diasRegistrados.get(j*2) && diasRegistrados.get(j*2+1)){

            if (DiasDeTrabajo.size() > 0) {
                DiasDeTrabajo.remove(spnDias.getSelectedItemPosition());
                if (DiasDeTrabajo.size() > 0) {

                    if (!diasRegistrados.get(j * 2)) {

                        btnRutaIdaVuelta.setText("ida");
                        btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorAccent));

                        diaFiltro = DiasDeTrabajo.get(0);
                        filter(DiasDeTrabajo.get(0), true);


                    } else {

                        btnRutaIdaVuelta.setText("vuelta");
                        btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorOrange));

                        diaFiltro = DiasDeTrabajo.get(0);
                        filter(DiasDeTrabajo.get(0), false);

                    }

                    ad.notifyDataSetChanged();
                }
            } else {
                finish();
            }
        } else if (diasRegistrados.get(j * 2) != diasRegistrados.get(j * 2 + 1)) {

            if (!diasRegistrados.get(j * 2)) {

                btnRutaIdaVuelta.setText("ida");
                btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorAccent));


                filter(DiasDeTrabajo.get(0), true);
                diaFiltro = DiasDeTrabajo.get(0);

            } else {

                btnRutaIdaVuelta.setText("vuelta");
                btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorOrange));


                filter(DiasDeTrabajo.get(0), false);
                diaFiltro = DiasDeTrabajo.get(0);
            }
        }
    }

    private void enviarInfoRutas() {

        for (int i = 0; i < rutaSeleccionadaList.size(); i++) {
            Ruta r = rutaSeleccionadaList.get(i);
            DatabaseNotificationSender databaseNotificationSender = new DatabaseNotificationSender();
            databaseNotificationSender.sendNotificacionRequest(this, r, r.getMiParada());
        }

    }

    private void filter(String text, boolean ida) {


        filteredList = new ArrayList<>();

        for (Ruta item : rutaDummyList) {
            if (item.getDia().toString().toLowerCase().contains(text.toLowerCase())
                    && (item.getIsLlegada() == ida)) {
                filteredList.add(item);
            }
        }

        adapter.filterList(filteredList);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        rvRutas.setLayoutAnimation(controller);


    }

    public void cambioIdaVuelta(View v) {


        if (DiasDeTrabajo.size() > 0) {

            if (btnRutaIdaVuelta.getText().toString() == "vuelta") {

                    btnRutaIdaVuelta.setText("ida");
                    btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorAccent));

            } else {

                    btnRutaIdaVuelta.setText("vuelta");
                    btnRutaIdaVuelta.setTextColor(getResources().getColor(R.color.colorOrange));


            }

            filter(diaFiltro, btnRutaIdaVuelta.getText() == "ida");

        }
        Log.i("RutasCandidatas",String.valueOf(DataHolder.getInstance().getUsuario().getRutasCandidatas()));

    }

    public void poblarLista() {

        rutaDummyList.addAll(DataHolder.getInstance().getUsuario().getRutasCandidatas());

    }


    @Override
    public void onNotificationSend(boolean operacionExitosa, Ruta ruta) {
        String rutaInfo = "";
        rutaInfo = rutaInfo.concat(
                "<br /><b>" + ruta.getDia().toString() + "</b>"
                        + " "
                        + String.valueOf(ruta.getParadas().get(0).getHora())
                        + ":" + String.format("%02d", ruta.getParadas().get(0).getMinuto())
                        + "<br />"
                        + "Conductor: " + String.valueOf(ruta.getChofer().getNombre())
                        + "<br />"
                        + "Con rumbo a: "
                        + String.valueOf(ruta.getParadas().get(ruta.getParadas().size() - 1).getLugar().getDireccion())
                        + "<br /> "
        );


        if (operacionExitosa) {

            new AlertDialog.Builder(this)
                    .setTitle("Se ha pedido la siguiente ruta")
                    .setMessage(Html.fromHtml(rutaInfo))
                    .setCancelable(false)
                    .setPositiveButton("Volver", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
            rutaSeleccionadaList.remove(ruta);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("No se ha registrado la siguiente ruta")
                    .setMessage(Html.fromHtml(rutaInfo))
                    .setCancelable(false)
                    .setPositiveButton("Volver", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .show();
        }

    }
}
