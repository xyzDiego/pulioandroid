package mx.itesm.pulio;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

public class DatabaseActualizacionRuta {

    private static final String TAG = "DBActualizacionRuta";

    //Método para actualizar la ruta después de que el chofer aceptó la notificacionRequest
    public void actualizarRuta(ActualizacionRutaListener actualizacionRutali, NotificacionRequest notificacionRequest) {
        Ruta ruta = notificacionRequest.getRuta();
        Usuario usuario = DataHolder.getInstance().getUsuario();
        WriteBatch writeBatch = FirebaseFirestore.getInstance().batch();

        DocumentReference rutaDocumentReference = FirebaseFirestore.getInstance().
                collection("organizacion").
                document(usuario.getOrganizacion().getId()).collection("rutas").
                document("r" + ruta.getId());

        //1. Actualizar número de inscritps
        writeBatch.update(rutaDocumentReference, "inscritos",  ruta.getInscritos());

        //2. Actualizar las paradas de la ruta en la base de datos
        for (int i = 0; i < ruta.getParadas().size(); i++) {
            DocumentReference paradaDocumentReference = rutaDocumentReference.
                    collection("paradas").
                    document("" + i);

            writeBatch.set(paradaDocumentReference, ruta.getParadas().get(i).toMap());

        }

        //3. Actualizar las referencias de los arreglos rutasLlegada y rutasSalida. Optimizándolo
        //solo se tiene que modificar para aquél pasajero que se añadió o dió de baja de la ruta
        for (int i = 0; i < ruta.getParadas().size(); i++) {
            if (ruta.getParadas().get(i).getPasajero() != null) {
                DocumentReference nuevoPasajeroDocumentReference =
                        FirebaseFirestore.getInstance().collection("usuario").
                        document(ruta.getParadas().get(i).getPasajero().getIdUsuario());

                if (ruta.getIsLlegada()) {
                    writeBatch.update(nuevoPasajeroDocumentReference, "rutasLlegada",
                            FieldValue.arrayUnion("r" + ruta.getId()));
                } else {
                    writeBatch.update(nuevoPasajeroDocumentReference, "rutasSalida",
                            FieldValue.arrayUnion("r" + ruta.getId()));
                }
            }
        }

        writeBatch.commit().addOnCompleteListener(
                new ActualizacionRuta(actualizacionRutali, notificacionRequest));
    }

    private class ActualizacionRuta implements OnCompleteListener<Void> {

        private final ActualizacionRutaListener actualizacionRutali;
        private final NotificacionRequest notificacionRequest;

        public ActualizacionRuta(ActualizacionRutaListener actualizacionRutali, NotificacionRequest notificacionRequest) {
            this.actualizacionRutali = actualizacionRutali;
            this.notificacionRequest = notificacionRequest;
        }

        @Override
        public void onComplete(@NonNull Task<Void> task) {
            if (task.isSuccessful()){
                Log.d(TAG,"Actualizacion Ruta Completada");
                actualizacionRutali.onRutaActualizada(true, notificacionRequest);

            }else{
                Log.d(TAG,"Actualizacion Ruta Fallida");
                actualizacionRutali.onRutaActualizada(false, notificacionRequest);

            }
        }
    }

    public interface ActualizacionRutaListener {
        void onRutaActualizada(boolean actualizacionExitosa, NotificacionRequest notificacionRequest);
    }

}
