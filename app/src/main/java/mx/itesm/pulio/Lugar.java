package mx.itesm.pulio;

public class Lugar {

    private String id;
    private final String direccion;
    private final double latitud;
    private final double longitud;

    public Lugar(String id, String direccion, double latitud, double longitud) {
        this.id = id;
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Lugar(String direccion, double latitud, double longitud) {
        this.direccion = direccion;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public String getId() {
        return id;
    }

    public String getDireccion() {
        return this.direccion;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Lugar {" +
                    "lugar: " + this.id + "\n" +
                    "direccion: " + this.direccion + "\n" +
                    "latitud: " + this.latitud + "\n" +
                    "longitud: " + this.longitud + "\n" +
                " }";
    }
}
