package mx.itesm.pulio;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.chad.pulioapp.R;

import java.util.ArrayList;

public class RegistroActiv extends AppCompatActivity implements DatabaseUserRegistration.UserRegistrationListener {

    private EditText eTNombre;
    private EditText eTCorreo;
    private EditText eTNoTel;
    private EditText eTContra;
    private EditText eTContra2;

    private Spinner spInstitucion;
    private ArrayAdapter<String> adapterInstitucion;
    private ProgressDialog progressDialog;

    private TextView tvNombreError;
    private TextView tvContraError;
    private TextView tvTelefonoError;
    private TextView tvCorreoError;

    private DatabaseUserRegistration dbUserRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        this.overridePendingTransition(R.anim.anim_slide_in_left,
                R.anim.anim_slide_out_left);

        eTNombre = findViewById(R.id.eTNombre);
        eTNombre.requestFocus();
        eTCorreo = findViewById(R.id.eTCorreo);
        eTNoTel = findViewById(R.id.eTNoTel);
        eTContra = findViewById(R.id.eTContra);
        eTContra2 = findViewById(R.id.eTContra2);

        eTCorreo.addTextChangedListener(new CorreoTextWatcher());

        spInstitucion = findViewById(R.id.spInstitucion);
        adapterInstitucion = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item);
        spInstitucion.setAdapter(adapterInstitucion);
        actualizarInstituciones();

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Cargando...");

        tvNombreError = findViewById(R.id.tvNombreError);
        tvContraError = findViewById(R.id.tvContraError);
        tvTelefonoError = findViewById(R.id.tvTelefonoError);
        tvCorreoError = findViewById(R.id.tvCorreoError);
        tvContraError = findViewById(R.id.tvContraError);

        tvNombreError.setText("");
        tvContraError.setText("");
        tvTelefonoError.setText("");
        tvCorreoError.setText("");

        tvNombreError.setTextSize(0f);
        tvContraError.setTextSize(0f);
        tvTelefonoError.setTextSize(0f);
        tvCorreoError.setTextSize(0f);

        dbUserRegistration = new DatabaseUserRegistration(this);
    }

    private void actualizarInstituciones(){
        ArrayList<String> list = new ArrayList<>();

        if (eTCorreo.getText().toString().split("@").length == 2){
            String dominio = "@" + eTCorreo.getText().toString().split("@")[1];
            for (Organizacion organizacion: dbUserRegistration.organizaciones) {
                if (organizacion.getDominio().equals(dominio))
                    list.add(organizacion.getNombre());
            }
        }

        if (list.isEmpty()) {
            list.add("No hay instituciones para tu correo");
        } else {
            list.add(0, "Seleciona una institución");
        }

        this.adapterInstitucion.clear();
        this.adapterInstitucion.addAll(list);
    }

    public void siguientePantalla(View v){
        if (checarDatos()) {
            verificarCorreo();
        }
        //Debug shorcut
        if (eTNombre.getText().toString().equals("6")){
            DataHolder.getInstance().setUsuario(new Usuario("test", "test", "test", null, 0));
            Intent n = new Intent(this, RegistroContActiv.class);
            startActivity(n);
        }
    }

    private void verificarCorreo() {
        dbUserRegistration.verificarCorreo(eTCorreo.getText().toString());
        progressDialog.show();
    }

    private boolean checarDatos() {
        boolean datosValidos = true;

        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        //Checar Nombre
        if (eTNombre.getText().toString().isEmpty()) {
            llamarError(eTNombre, tvNombreError, "*Campo requerido", shake);
            datosValidos = false;
        } else if (!eTNombre.getText().toString().matches("[a-zA-Z\\u00C0-\\u017F\\p{Zs}]+[' '][a-zA-Z\\u00C0-\\u017F]+")) {
            llamarError(eTNombre, tvNombreError, "*Solo debe contener letras (Nombre y Apellido)", shake);
            datosValidos = false;
        } else {
            resetearError(tvNombreError);
        }

        //Checar Correo
        if (eTCorreo.getText().toString().isEmpty()) {
            llamarError(eTCorreo, tvCorreoError, "*Campo requerido", shake);
            datosValidos = false;
        } else if (!eTCorreo.getText().toString().matches("^[\\w\\d_\\.-]+@([\\w\\-]+\\.)+[a-z]{2,4}$")) {
            llamarError(eTCorreo, tvCorreoError, "*Ingresar un correo valido", shake);
            datosValidos = false;
        } else {
            resetearError(tvCorreoError);
        }

        //Checar Contraseña
        if (eTContra.getText().toString().isEmpty()) {
            llamarError(eTContra, tvContraError, "*Campo requerido", shake);
            llamarError(eTContra2, tvContraError, "*Campo requerido", shake);
            datosValidos = false;
        } else if (eTContra2.getText().toString().isEmpty()) {
            llamarError(eTContra, tvContraError, "*Confirmar contraseña", shake);
            llamarError(eTContra2, tvContraError, "*Confirmar contraseña", shake);
            datosValidos = false;
        } else if (!eTContra2.getText().toString().equals(eTContra.getText().toString())) {
            llamarError(eTContra, tvContraError, "*Contraseña es diferente", shake);
            llamarError(eTContra2, tvContraError, "*Contraseña es diferente", shake);
            datosValidos = false;
        } else if (eTContra2.getText().toString().length() < 8) {
            llamarError(eTContra, tvContraError, "*Contraseña debe ser de minimo 8 caracteres", shake);
            llamarError(eTContra2, tvContraError, "*Contraseña debe ser de minimo 8 caracteres", shake);
            datosValidos = false;
        } else if (!eTContra2.getText().toString().matches(".*\\d+.*")) {
            llamarError(eTContra, tvContraError, "*Contraseña debe tener al menos un numero", shake);
            llamarError(eTContra2, tvContraError, "*Contraseña debe tener al menos un numero", shake);
            datosValidos = false;
        } else {
            resetearError(tvContraError);
        }

        //Checar No Telefonico
        if (eTNoTel.getText().toString().isEmpty()) {
            llamarError(eTNoTel, tvTelefonoError, "*Campo requerido", shake);
            datosValidos = false;
        } else if (eTNoTel.getText().toString().length() != 10) {
            llamarError(eTNoTel, tvTelefonoError, "*Ingresar un teléfono valido", shake);
            datosValidos = false;
        } else {
            resetearError(tvTelefonoError);
        }

        //Checar Institucion
        if (spInstitucion.getSelectedItemPosition() == 0) {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setMessage("Seleccione una institución")
            .setPositiveButton("Aceptar",null)
                    .show();


            spInstitucion.setAnimation(shake);
            datosValidos = false;
        }

        return datosValidos;
    }

    private void resetearError(TextView t){
        t.setText("");
        t.setTextSize(0f);
    }

    private void llamarError(EditText et, TextView t, String error, Animation a){
        et.startAnimation(a);
        t.setText(error);
        t.setTextColor(Color.RED);
        t.setTextSize(10f);
    }

    @Override
    public void onMailVerified(boolean mailValido) {
        if (!mailValido) {
            llamarError(eTCorreo, tvCorreoError, "*Esta cuenta ya existe",
                    AnimationUtils.loadAnimation(this, R.anim.shake));
            progressDialog.dismiss();
        } else {
            dbUserRegistration.registrarUsuario(eTNombre.getText().toString(),
                    eTCorreo.getText().toString(), eTContra.getText().toString(),
                    eTNoTel.getText().toString(), spInstitucion.getSelectedItem().toString());
        }
    }

    @Override
    public void onRegisterCompleted(boolean registroExitoso) {
        progressDialog.dismiss();
        if (registroExitoso) {
            Intent intent = new Intent(this, RegistroContActiv.class);
            startActivity(intent);
        }
        else {
            new AlertDialog.Builder(this).
                    setMessage("No hemos podido registrarte en la base de datos. Inténtalo de nuevo.").
                    setNeutralButton("Entendido", null).show();
        }
    }

    private class CorreoTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            actualizarInstituciones();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.anim_slide_in_right,
                R.anim.anim_slide_out_right);
    }
}
