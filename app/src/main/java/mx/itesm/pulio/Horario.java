package mx.itesm.pulio;

import java.util.HashMap;
import java.util.Map;

public class Horario {

    private final Lugar lugar;
    private final int hora;
    private final int minuto;

    public Horario(Lugar lugar, int hora, int minuto) {
        this.lugar = lugar;
        this.hora = hora;
        this.minuto = minuto;
    }

    // Recibe la hora en formato "H:mm"
    public Horario(Lugar lugar, String hora) {
        this.lugar = lugar;

        // Se corta el string de la hora, se convierten a entero y se asignan
        String[] horaDesglosada = hora.split(":");
        this.hora = Integer.parseInt(horaDesglosada[0]);
        this.minuto = Integer.parseInt(horaDesglosada[1]);
    }

    public int getHora() {
        return hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getMinutosTotales() {
        return hora * 60 + minuto;
    }

    public Lugar getLugar() {
        return lugar;
    }

    public String formatTime() {
        return String.format("%02d:%02d", this.hora, this.minuto);
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("idLugar", this.lugar.getId());
        map.put("hora", this.hora);
        map.put("minuto", this.minuto);
        return map;
    }

    @Override
    public String toString() {
        return "Horario{" + "\n" +
                "lugar=" + lugar + "\n" +
                "hora=" + hora + "\n" +
                "minuto=" + minuto + "\n" +
                '}';
    }
}
