package mx.itesm.pulio;

public class NotificationAcknowledge extends Notificacion {

    //Ei el booleano es verdadero significa que el chofer aceptó la solicitud del usuario, de lo
    //contrario significa que la declino
    private final boolean aceptado;
    private final String idRuta;
    //El nombre del chofer que aceptó/rechazó la solicitud
    private String choferNombre;
    //Día de la ruta, y hora y minuto de la parada a la que el usuario se inscribió y el chofer
    //aceptó/rechazó
    private String dia;
    private int hora;
    private int minuto;

    public NotificationAcknowledge(String idRemitente, String idDestinatario, boolean aceptado, String idRuta,
                                   String choferNombre, String dia, int hora, int minuto) {
        super(idRemitente, idDestinatario, false);
        this.aceptado = aceptado;
        this.idRuta = idRuta;
        this.choferNombre = choferNombre;
        this.dia = dia;
        this.hora = hora;
        this.minuto = minuto;
    }

    public boolean isAceptado() {
        return aceptado;
    }

    public String getChoferNombre() {
        return choferNombre;
    }

    public String getDia() {
        return dia;
    }

    public int getHora() {
        return hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public String getIdRuta() {
        return idRuta;
    }

    @Override
    public String toString() {
        return "NotificationAcknowledge{" +
                "aceptado=" + aceptado +
                ", choferNombre='" + choferNombre + '\'' +
                ", dia='" + dia + '\'' +
                ", hora=" + hora +
                ", minuto=" + minuto +
                '}';
    }
}
