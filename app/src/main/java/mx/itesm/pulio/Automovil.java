package mx.itesm.pulio;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Automovil {

    private final String id;
    private String modelo;
    private int anio;
    private int capacidad;
    private Boolean[] disponibilidadPorDia;

    public Automovil(String id) {
        this.id = id;
        this.modelo = null;
        this.anio = 0;
        this.capacidad = 0;
        this.disponibilidadPorDia = new Boolean[6];
        Arrays.fill(this.disponibilidadPorDia, false);
    }

    public String getId() {
        return id;
    }

    public String getModelo() {
        return modelo;
    }

    public int getAnio() {
        return anio;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public Boolean[] getDisponibilidadPorDia() {
        return disponibilidadPorDia;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public void setDisponibilidadPorDia(Boolean[] disponibilidadPorDia) {
        this.disponibilidadPorDia = disponibilidadPorDia;
    }

    public boolean esUtilizado() {
        for (int i = 0; i < this.disponibilidadPorDia.length; i++) {
            if (this.disponibilidadPorDia[i])
                return true;
        }

        return false;
    }

    public void clear() {
        this.modelo = null;
        this.anio = 0;
        this.capacidad = 0;
        Arrays.fill(this.disponibilidadPorDia, false);
    }

    @Override
    public String toString() {
        return "Automovil{" +
                "id='" + id + "\n" +
                "modelo='" + modelo + "\n" +
                "anio=" + anio + "\n" +
                "capacidad=" + capacidad + "\n" +
                "disponibilidadPorDia=" + Arrays.toString(disponibilidadPorDia) + "\n" +
                '}';
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("modelo", this.modelo);
        map.put("anio", this.anio);
        map.put("capacidad", this.capacidad);
        map.put("disponibilidadPorDia", Arrays.asList(disponibilidadPorDia));
        return map;
    }
}
