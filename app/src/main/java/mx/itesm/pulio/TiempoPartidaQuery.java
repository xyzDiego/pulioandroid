package mx.itesm.pulio;

/**
 * Dado un punto de origen y de destino y un tiempo de llegada, calcula el tiempo de salida,
 *  y la distancia entre ambos puntos.
 */

public class TiempoPartidaQuery extends TiempoQuery {

    private int origenHora;
    private int origenMinuto;
    private int destinoHora;
    private int destinoMinuto;

    public TiempoPartidaQuery(double origenLatitud, double origenLongitud, double destinoLatitud, double destinoLongitud,
                              int destinoHora, int destinoMinuto, DiaDeLaSemana diaDeLaSemana) {
        super(origenLatitud, origenLongitud, destinoLatitud, destinoLongitud, diaDeLaSemana);
        this.destinoHora = destinoHora;
        this.destinoMinuto = destinoMinuto;
    }

    @Override
    public void obtenerInformacionDuracion(String strDuration) {
        int duration = Integer.parseInt(strDuration.split(" ")[0]);
        int tiempoMinutosDestino = (this.destinoHora * 60) + this.destinoMinuto;
        int tiempoMinutosOrigen = tiempoMinutosDestino - duration;

        this.origenHora = tiempoMinutosOrigen / 60;
        this.origenMinuto = tiempoMinutosOrigen % 60;
    }

    public int getOrigenHora() {
        return origenHora;
    }

    public int getOrigenMinuto() {
        return origenMinuto;
    }

    @Override
    public String toString() {
        return super.toString() + " " +
                "TiempoPartidaQuery{" +
                "origenHora=" + origenHora +
                ", origenMinuto=" + origenMinuto +
                ", destinoHora=" + destinoHora +
                ", destinoMinuto=" + destinoMinuto +
                '}';
    }
}
