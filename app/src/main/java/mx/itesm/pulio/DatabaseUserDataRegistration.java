package mx.itesm.pulio;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DatabaseUserDataRegistration {

    private static final String TAG = "DBUserDataRegistration";

    private final Usuario usuario;
    private final DocumentReference usuarioRef;
    private final DocumentReference orgRef;
    private final DocumentReference ultimosIndicesRef;
    private final UserDataRegistrationListener udrli;
    private int indiceRuta;

    public DatabaseUserDataRegistration(UserDataRegistrationListener udrli) {
        this.usuario = DataHolder.getInstance().getUsuario();
        this.usuarioRef = FirebaseFirestore.getInstance().collection("usuario").
                document(this.usuario.getId());
        this.orgRef = FirebaseFirestore.getInstance().collection("organizacion").
                document(usuario.getOrganizacion().getId());
        this.ultimosIndicesRef = FirebaseFirestore.getInstance().collection("indices").
                document("ultimoIndice");
        this.udrli = udrli;
    }

    public void registrarDatosUsuario() {
        if (usuario.getRutasLlegada().size() > 0 || usuario.getRutasSalida().size() > 0) {
            //Se obtiene y actualiza el último índice de la ruta.
            ultimosIndicesRef.get().addOnCompleteListener(new RutaIndexRetrieval());
        } else {
            registrarDatos();
        }
    }

    private class RutaIndexRetrieval implements OnCompleteListener<DocumentSnapshot> {
        @Override
        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();

                indiceRuta = Integer.parseInt(document.getData().get("ruta").toString());
                ultimosIndicesRef.update("ruta", indiceRuta +
                        usuario.getRutasSalida().size() + usuario.getRutasLlegada().size()).
                        addOnCompleteListener(new RutaIndexUpdate());
            } else {
                Log.d(TAG, "Falla al intentar obterner el último índice de la ruta");
                udrli.onUserDataRegistrationCompleted(false);
            }
        }
    }

    private class RutaIndexUpdate implements OnCompleteListener<Void> {
        @Override
        public void onComplete(@NonNull Task<Void> task) {
            if (task.isSuccessful()) {
                registrarDatos();
            } else {
                Log.d(TAG, "Falla al intentar actualizar los índices de la ruta");
                udrli.onUserDataRegistrationCompleted(false);
            }
        }
    }

    private void registrarDatos() {
        WriteBatch writeBatch = FirebaseFirestore.getInstance().batch();

        //1 Registro Automóvil
        Automovil automovil = usuario.getAutomovil();
        if (automovil.getModelo() != null) {
            //El usuario tiene automóvil
            DocumentReference documentReferenceAutomovil =
                    usuarioRef.collection("automovil").document(automovil.getId());
            writeBatch.set(documentReferenceAutomovil, automovil.toMap());
        }

        //2 Registro Lugares
        for (Lugar lugar: usuario.getLugares()) {
            DocumentReference documentReferenceLugar =
                    usuarioRef.collection("lugares").document(lugar.getId());
            writeBatch.set(documentReferenceLugar, lugar);
        }

        //3 Registro Horario
        for (int i = 0; i < DiaDeLaSemana.TOTAL_DIAS.ordinal(); i++) {
            String dia = DiaDeLaSemana.values()[i].name();
            if (usuario.getHorariosLlegada()[i] != null) {
                DocumentReference documentReferenceHorarioLlegada =
                        usuarioRef.collection("horariosLlegada").document(dia);
                writeBatch.set(documentReferenceHorarioLlegada, usuario.getHorariosLlegada()[i].toMap());
            }
            if (usuario.getHorariosSalida()[i] != null) {
                DocumentReference documentReferenceHorarioSalida =
                        usuarioRef.collection("horariosSalida").document(dia);
                writeBatch.set(documentReferenceHorarioSalida, usuario.getHorariosSalida()[i].toMap());
            }
        }

        //4 Registro Rutas Llegada y Salida en la colección global de rutas de la organización
        ArrayList<Ruta> rutasTodas = new ArrayList<>();
        rutasTodas.addAll(usuario.getRutasLlegada());
        rutasTodas.addAll(usuario.getRutasSalida());
        for (Ruta ruta: rutasTodas) {
            ruta.setId(indiceRuta++);
            DocumentReference documentReferenceRuta = orgRef.collection("rutas").
                    document("r" + ruta.getId());

            writeBatch.set(documentReferenceRuta, ruta.toMap());
            //4.1 Registro Chofer
            writeBatch.set(documentReferenceRuta, ruta.getChofer().toMap(), SetOptions.merge());
            //4.2 Registro Paradas
            CollectionReference collectionReferenceParadas = documentReferenceRuta.collection("paradas");
            for (int indexParada = 0; indexParada < ruta.getParadas().size(); indexParada++) {
                DocumentReference documentReferenceParada = collectionReferenceParadas.document("" + indexParada);
                writeBatch.set(documentReferenceParada, ruta.getParadas().get(indexParada).toMap());
            }
        }

        //5 Registro Rutas Llegada en el usuario
        String[] indicesRutasLlegada = new String[usuario.getRutasLlegada().size()];
        for (int i = 0; i < usuario.getRutasLlegada().size(); i++) {
            indicesRutasLlegada[i] = "r" + usuario.getRutasLlegada().get(i).getId();
        }
        Map<String, Object> mapRutasLlegada = new HashMap<>();
        mapRutasLlegada.put("rutasLlegada", Arrays.asList(indicesRutasLlegada));
        writeBatch.set(usuarioRef, mapRutasLlegada, SetOptions.merge());

        //6 Registro Rutas Salida en el usuario
        String[] indicesRutasSalida = new String[usuario.getRutasSalida().size()];
        for (int i = 0; i < usuario.getRutasSalida().size(); i++) {
            indicesRutasSalida[i] = "r" + usuario.getRutasSalida().get(i).getId();
        }
        Map<String, Object> mapRutasSalida = new HashMap<>();
        mapRutasSalida.put("rutasSalida", Arrays.asList(indicesRutasSalida));
        writeBatch.set(usuarioRef, mapRutasSalida, SetOptions.merge());

        writeBatch.commit().addOnCompleteListener(new UserDataRegistration());
    }


    private class UserDataRegistration implements OnCompleteListener<Void> {

        @Override
        public void onComplete(@NonNull Task<Void> task) {
            udrli.onUserDataRegistrationCompleted(task.isSuccessful());
        }
    }

    public interface UserDataRegistrationListener {
        void onUserDataRegistrationCompleted(boolean exito);
    }

}
