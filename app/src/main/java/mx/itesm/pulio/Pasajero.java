package mx.itesm.pulio;

public class Pasajero {

    private final String idUsuario;
    private final String nombre;
    private final long telefono;

    public Pasajero(String idUsuario, String nombre, long telefono) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public long getTelefono() {
        return telefono;
    }

    @Override
    public String toString() {
        return "Pasajero{" +
                "idUsuario='" + idUsuario + '\'' +
                ", nombre='" + nombre + '\'' +
                ", telefono=" + telefono +
                '}';
    }
}
