package mx.itesm.pulio;

import java.util.HashMap;
import java.util.Map;

public class Parada {

    private Lugar lugar;
    private Pasajero pasajero;
    //Las variables hora y minuto indican el tiempo aproximado en el que se recogerá al pasajero
    private int hora;
    private int minuto;
    //La variable distanciaKm indica el total de kilómetros a los que se ubica esta parada
    //con respecto a la parada anterior. En caso de ser la primera parada este valor es de 0.
    private double distanciaKm;

    public Parada() { }

    public Parada(Lugar lugar, Pasajero pasajero, int hora, int minuto, double distanciaKm) {
        this.lugar = lugar;
        this.pasajero = pasajero;
        this.hora = hora;
        this.minuto = minuto;
        this.distanciaKm = distanciaKm;
    }

    public Parada(Lugar lugar, Pasajero pasajero) {
        this.lugar = lugar;
        this.pasajero = pasajero;
    }

    //Constructor de copia
    public Parada(Parada parada) {
        this.lugar = parada.getLugar();
        this.pasajero = parada.getPasajero();
        this.hora = parada.getHora();
        this.minuto = parada.getMinuto();
        this.distanciaKm = parada.getDistanciaKm();
    }

    public Lugar getLugar() {
        return lugar;
    }

    public Pasajero getPasajero() {
        return pasajero;
    }

    public int getHora() {
        return hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public int getMinutosTotales() {
        return hora * 60 + minuto;
    }

    public double getDistanciaKm() {
        return distanciaKm;
    }

    public void setLugar(Lugar lugar) {
        this.lugar = lugar;
    }

    public void setPasajero(Pasajero pasajero) {
        this.pasajero = pasajero;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public void setDistanciaKm(double distanciaKm) {
        this.distanciaKm = distanciaKm;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("lugarId", this.getLugar().getId());
        map.put("lugarDireccion", this.getLugar().getDireccion());
        map.put("lugarLatitud", this.getLugar().getLatitud());
        map.put("lugarLongitud", this.getLugar().getLongitud());
        map.put("idPasajero", this.getPasajero() == null ? "null" : this.getPasajero().getIdUsuario());
        map.put("pasajeroNombre", this.getPasajero() == null ? "null" : this.getPasajero().getNombre());
        map.put("pasajeroTelefono", this.getPasajero() == null ? "null" :this.getPasajero().getTelefono());
        map.put("hora", this.getHora());
        map.put("minuto", this.getMinuto());
        map.put("distanciaKm", this.getDistanciaKm());
        return map;
    }

    @Override
    public String toString() {
        return "Parada{" + "\n" +
                "lugar=" + lugar + "\n" +
                "pasajero=" + pasajero + "\n" +
                "hora=" + hora + "\n" +
                "minuto=" + minuto + "\n" +
                "distanciaKm=" + distanciaKm + "\n" +
                '}';
    }


}
