package mx.itesm.pulio;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Ruta implements Comparable {

    private static final String TAG = "RUTA";

    private final DiaDeLaSemana dia;
    //Indica si es una ruta que es hacia el trabajo (isLlegada=true) o si es una ruta de regreso del
    //trabaho (isLlegada=false)
    private final boolean isLlegada;
    private final Chofer chofer;
    //Para una ruta de llegada la primera parada tiene como pasajero al chofer y de lugar
    //el lugar del chofer y la última paradad tiene como pasajero null y de lugar el lugar de la
    //organización. Mientras que para una ruta de salida la primera parada tiene como pasajero null
    //y de lugar el lugar de la organización y la última parada tiene como pasajero el chofer
    //y de lugar el lugar del chofer.
    private final ArrayList<Parada> paradas;
    private int inscritos;
    //El id se le asigna a la ruta hasta cuando se sube a la base de datos
    private int id;

    //Constructor utilizado únicamente para crear nuevas Rutas al terminar el proceso de registro
    public Ruta(DiaDeLaSemana dia, boolean isLlegada, Chofer chofer) {
        this.dia = dia;
        this.isLlegada = isLlegada;
        this.chofer = chofer;
        this.paradas = new ArrayList<>();
        this.inscritos = 1;
    }

    //Constructor utilizado únicamente para crear rutas existentes durante el proceso de importar rutas de la organización
    public Ruta(DiaDeLaSemana dia, boolean isLlegada, Chofer chofer, int inscritos, int id) {
        this.dia = dia;
        this.isLlegada = isLlegada;
        this.chofer = chofer;
        this.paradas = new ArrayList<>();
        this.inscritos = inscritos;
        this.id = id;
    }

    //Constructor de copia
    public Ruta(Ruta ruta) {
        this.dia = ruta.getDia();
        this.isLlegada = ruta.getIsLlegada();
        this.chofer = ruta.getChofer();
        this.paradas = new ArrayList<>(ruta.getParadas().size());
        for (Parada parada : ruta.getParadas())
            this.paradas.add(new Parada(parada));
        this.inscritos = ruta.getInscritos();
        this.id = ruta.getId();
    }

    //Calcula la distancia total en kilómetros de cada parada
    public double obtenerDistanciaTotal() {
        double distanciaKmTotal = 0;
        for (Parada parada : getParadas())
            distanciaKmTotal += parada.getDistanciaKm();

        return distanciaKmTotal;
    }

    //Calcula el tiempo total en minutos. El valor se obtiene de la diferencia del tiempo de la
    //última parada y el tiempo de la primera parada
    public int obtenerTiempoTotal() {
        Parada primeraParada = getParadas().get(0);
        Parada ultimaParada = getParadas().get(getParadas().size() - 1);

        int minutosPrimeraParada = (primeraParada.getHora() * 60) + primeraParada.getMinuto();
        int minutosUltimaParada = (ultimaParada.getHora() * 60) + ultimaParada.getMinuto();

        return minutosUltimaParada - minutosPrimeraParada;
    }

    public DiaDeLaSemana getDia() {
        return dia;
    }

    public boolean getIsLlegada() {
        return isLlegada;
    }

    public Chofer getChofer() {
        return chofer;
    }

    public ArrayList<Parada> getParadas() {
        return paradas;
    }

    public int getInscritos() {
        return inscritos;
    }

    public int getId() {
        return id;
    }

    public void setInscritos(int inscritos) {
        this.inscritos = inscritos;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("dia", this.getDia().name());
        map.put("isLlegada", this.getIsLlegada());
        map.put("inscritos", this.getInscritos());
        return map;
    }

    @Override
    public String toString() {
        return "Ruta{" + "\n" +
                "dia=" + dia + "\n" +
                "isLlegada=" + isLlegada + "\n" +
                "chofer=" + chofer + "\n" +
                "paradas=" + paradas + "\n" +
                "inscritos=" + inscritos + "\n" +
                "id=" + id + "\n" +
                '}';
    }

    public void agregarParadaIntermedia(Parada parada) {
        this.paradas.add(1, parada);
    }

    public void recalcularDistanciasYTiemposRutaLlegada(Boolean opearcionExitosa) {
        recalcularDistanciasYTiemposRutaLlegada(this.getParadas().size() - 1,
                opearcionExitosa);
    }

    //Método recursivo que va calculando distancias y tiempos de cada una de las paradas de una ruta
    //de llegada. Iniciando desde el último par de paradas hasta el primer par de paradas.
    private void recalcularDistanciasYTiemposRutaLlegada(int indiceParadaB, Boolean operacionExitosa) {
        int indiceParadaA = indiceParadaB - 1;
        if (indiceParadaA == -1)
            return;
        Parada paradaA = this.getParadas().get(indiceParadaA);
        Parada paradaB = this.getParadas().get(indiceParadaB);

        TiempoPartidaQuery tiempoPartidaQuery = new TiempoPartidaQuery(paradaA.getLugar().getLatitud(),
                paradaA.getLugar().getLongitud(), paradaB.getLugar().getLatitud(), paradaB.getLugar().getLongitud(),
                paradaB.getHora(), paradaB.getMinuto(), this.dia);

        tiempoPartidaQuery.start();
        try {
            tiempoPartidaQuery.join();
        } catch (InterruptedException e) {
            Log.e(TAG, "Excepción lanzada con tiempoPartidaQuery " + tiempoPartidaQuery.toString()
            + " " + e.getMessage());
            operacionExitosa = false;
        }

        if (tiempoPartidaQuery.getStatus() == StatusRequest.OK) {
            paradaA.setHora(tiempoPartidaQuery.getOrigenHora());
            paradaA.setMinuto(tiempoPartidaQuery.getOrigenMinuto());
            paradaB.setDistanciaKm(tiempoPartidaQuery.getDistanciaKm());
        } else {
            Log.e(TAG, "Error realizando tiempoPartidaQuery " + tiempoPartidaQuery.toString());
            paradaA.setHora(0);
            paradaA.setMinuto(0);
            paradaB.setDistanciaKm(0);
            operacionExitosa = false;
        }

        recalcularDistanciasYTiemposRutaLlegada(indiceParadaB - 1, operacionExitosa);
    }

    public void recalcularDistanciasYTiemposRutaSalida(Boolean operacionExitosa) {
        recalcularDistanciasYTiemposRutaSalida(0, operacionExitosa);
    }

    //Método recursivo que va calculando distancias y tiempos de cada una de las paradas de una ruta
    //de salida. Iniciando del primer par de paradas hasta el último par de paradas.
    private void recalcularDistanciasYTiemposRutaSalida(int indiceParadaA, Boolean operacionExitosa) {
        int indiceParadaB = indiceParadaA + 1;
        if (indiceParadaB == this.getParadas().size())
            return;
        Parada paradaA = this.getParadas().get(indiceParadaA);
        Parada paradaB = this.getParadas().get(indiceParadaB);

        TiempoSalidaQuery tiempoSalidaQuery = new TiempoSalidaQuery(paradaA.getLugar().getLatitud(),
                paradaA.getLugar().getLongitud(), paradaB.getLugar().getLatitud(), paradaB.getLugar().getLongitud(),
                paradaA.getHora(), paradaA.getMinuto(), this.dia);

        tiempoSalidaQuery.start();
        try {
            tiempoSalidaQuery.join();
        } catch (InterruptedException e) {
            Log.e(TAG, "Excepción lanzada con tiempoSalidaQuery " + tiempoSalidaQuery.toString()
                    + " " + e.getMessage());
            operacionExitosa = false;
        }

        if (tiempoSalidaQuery.getStatus() == StatusRequest.OK) {
            paradaB.setHora(tiempoSalidaQuery.getDestinoHora());
            paradaB.setMinuto(tiempoSalidaQuery.getDestinoMinuto());
            paradaB.setDistanciaKm(tiempoSalidaQuery.getDistanciaKm());

        } else {
            Log.e(TAG, "Error realizando tiempoSalidaQuery " + tiempoSalidaQuery.toString());
            paradaB.setHora(0);
            paradaB.setMinuto(0);
            paradaB.setDistanciaKm(0);
            operacionExitosa = false;
        }

        recalcularDistanciasYTiemposRutaSalida(indiceParadaA + 1, operacionExitosa);
    }

    @Override
    public int compareTo(Object object) {
        Ruta ruta = (Ruta) object;

        if (ruta.getDia() != this.getDia())
            return this.getDia().ordinal() - ruta.getDia().ordinal();

        return this.getParadas().get(0).getMinutosTotales() - ruta.getParadas().get(0).getMinutosTotales();
    }

    public int getMiParada(){
        for (int i = 1; i < getParadas().size() - 1; i++){
            if (getParadas().get(i).getPasajero().getNombre().equals(DataHolder.getInstance().getUsuario().getNombre())){
                return i;
            }
        }

        return -1;
    }

}
